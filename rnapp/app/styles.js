 
import { COLOR } from "react-native-material-ui"

export const uiTheme = {
	palette: {
		primaryColor: COLOR.red500 ,
		secondaryColor: COLOR.red700 ,
		accentColor: COLOR.amber800,
		alertColor:    COLOR.amber600,
	},
	toolbar: {
		container: {
			height: 50,
		},
	},
}