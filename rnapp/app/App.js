"use strict";
import React, { Component } from "react";
import Meteor, { createContainer } from "react-native-meteor";
import SignInForm from "./components/SignInForm";
import { ThemeProvider } from "react-native-material-ui";
import NotConnected from "./components/NotConnected";
import { View, Image, AsyncStorage } from "react-native";
import Navigation from "./components/Navigation";
import { uiTheme } from "./styles";
import { MenuContext } from "react-native-popup-menu";
import WithDatabase from "./WithDatabase";

 
Meteor.connect("ws://192.168.1.103:3000/websocket");

class App extends Component {
  constructor(props) {
    super(props);

    this.renderPage = this.renderPage.bind(this);
    this.handlePressOffline = this.handlePressOffline.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.setCurrentUser = this.setCurrentUser.bind(this);

    this.state = { currentUser: null };
    this.setCurrentUser(props.currentUser);
    removeInvalidLocalStorageEntries();
  }

  setCurrentUser(currentUser) {
    this.state.currentUser = currentUser || null;
    AsyncStorage.getItem("reactnativemeteor_usertoken", (err, result) => {
      if (!err && result === "offline") {
        this.setState({
          currentUser: { name: "Offline", serviceType: "offline" }
        });
      }
    });
  }

  componentWillReceiveProps(props) {
    if (props.currentUser && props.currentUser._id) {
      this.setState({ currentUser: props.currentUser });
    }
  }

  static propTypes() {
    return {
      loggingIn: React.PropTypes.bool,
      status: React.PropTypes.object.isRequired,
      currentUser: React.PropTypes.object.isRequired
    };
  }

  handlePressOffline() {
    this.setState({ currentUser: { name: "Offline", serviceType: "offline" } });
    AsyncStorage.setItem("reactnativemeteor_usertoken", "offline");
  }

  renderSignInForm() {
    if (this.props.status.connected) {
      return (
        <SignInForm
          isOnline={this.props.status.connected}
          onPressOffline={this.handlePressOffline}
        />
      );
    } else {
      return (
        <NotConnected status={this.props.status || { status: "connecting" }} />
      );
    }
  }

  handleLogout() {
    if (this.state.currentUser.serviceType === "offline") {
      AsyncStorage.removeItem("reactnativemeteor_usertoken");
    } else {
      Meteor.logout();
    }
    this.setState({ currentUser: null });
  }

  renderPage() {
    if (this.state.currentUser) {
      return (
        <Navigation
          db={this.props.db}
          currentUser={this.state.currentUser}
          status={this.props.status}
          onLogout={this.handleLogout}
        />
      );
    }

    return (
      <Image
        style={{
          flex: 1,
          alignItems: "center",
          resizeMode: "cover",
          width: undefined,
          height: undefined
        }}
        source={require("./images/bg.jpg")}
      >
        {this.renderSignInForm()}
      </Image>
    );
  }

  render() {
    return (
      <ThemeProvider uiTheme={uiTheme}>
        <MenuContext>
          <View style={{ flex: 1 }}>
            {this.renderPage()}
          </View>
        </MenuContext>
      </ThemeProvider>
    );
  }
}

export default WithDatabase(
  createContainer(
    () => {
      const userHandle = Meteor.subscribe("currentUserData");
      return {
        status: Meteor.status(),
        currentUser: Meteor.user(),
        loggingIn: Meteor.loggingIn()
      };
    },
    App
  )
);

const removeInvalidLocalStorageEntries = () => {
  const whitelist = [
    "reactnativemeteor_usertoken",
    "DialectsDb",
    "TranscriptionsDb"
  ];

  AsyncStorage.getAllKeys((err, keys) => {
    if (!err) {
      keys.forEach(k => {
        if (!whitelist.includes(k)) {
          AsyncStorage.removeItem(k, err => {
            if (err) {
              console.log("Failed to remove:" + k);
            } else {
              console.log("Removed: " + k);
            }
          });
        }
      });
    } else {
      console.log("Error getting keys from async storage");
    }
  });
};
