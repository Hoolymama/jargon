import Meteor from "react-native-meteor";

const fetchMyDialects = (local) => {


  this.props.DialectsDb.find({}, (err, result) => {
      if (!err) {
        const dialects = this.insertDialectType(result, "client");
        this.setState({ dialectsClient: dialects });
      }
    });
    Meteor.call("getDialectsMine", (err, result) => {
      if (!err) {
        const dialects = this.insertDialectType(result, "server");
        this.setState({ dialectsServer: dialects });
      }
    });

}


function otherChatParticipant(chat, currentUserId){
	if (chat.participants[0]._id === currentUserId) {
		return  chat.participants[1]
	}
	return  chat.participants[0]
}

function currentUserChatParticipant(chat, currentUserId){
	if (chat.participants[0]._id === currentUserId) {
		return  chat.participants[0]
	}
	return  chat.participants[1]
}


function lastChatMessage(chat){
	if (! chat.messages) { return "" }
	return chat.messages[(chat.messages.length-1)]
}

function iHaveSeen(userId, chat){
	const participant = chat.participants.find((p) => {
		return p._id === userId
	})
	return participant.seen
}


function	createUserSummary(user){
	return {
		_id:user._id,
		name: user.name,
		avatar: user.avatar,
		color: user.color
	}
}
function	userSummaryFromRemark(remark){
	return {
		_id:remark.ownerId,
		name: remark.ownerName,
		avatar: remark.ownerAvatar,
		color: remark.ownerColor
	}
}

function	randomUUID(){
	return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
		var r = Math.random()*16|0, v = c == "x" ? r : (r&0x3|0x8)
		return v.toString(16)
	})
}

 

function userChanged(user, nextUser){
	const geoEpsilon = 0.0000001 
	const result = {}
	result.name = (user.name  != nextUser.name)
	result.wordThreshold = (user.wordThreshold  != nextUser.wordThreshold)
	result.timeThreshold = (user.timeThreshold  != nextUser.timeThreshold)
	result.distanceThreshold = (user.distanceThreshold  != nextUser.distanceThreshold)
	result.avatar = (user.avatar  != nextUser.avatar)
	result.needsFetch = (user.needsFetch  != nextUser.needsFetch)
	
	result.location = (
		(! user.location)  || 	(! nextUser.location) ||
		(Math.abs(user.location.coordinates[0]  - nextUser.location.coordinates[0]) > geoEpsilon) ||
		(Math.abs(user.location.coordinates[1]  - nextUser.location.coordinates[1]) > geoEpsilon)
	)

	result.remarksIdentityParams = (
		result.name ||
		result.avatar
	)

	result.remarksSearchParams = (
		result.wordThreshold ||
		result.timeThreshold ||
		result.distanceThreshold ||
		result.location ||
		result.needsFetch
	)
	return result
}

export {
	userChanged,
	 otherChatParticipant,
	 lastChatMessage,
	 createUserSummary,
	 randomUUID,
	 iHaveSeen,
	 currentUserChatParticipant,
	 userSummaryFromRemark
	}

