const cloneObject = (o, func) => {
  return JSON.parse(JSON.stringify(o, func));
};
 
const insertDialectType = (dialects, type) => {
  let result = cloneObject(dialects);
  if (Array.isArray(result)) {
    result = dialects.map(d => {
      d.type = type;
      return d;
    });
  }
  return result;
}



const removeTimestamps = (key, value) => {
  if (key === "updatedAt" || key === "createdAt") {
    return undefined;
  }
  return value;
};

const removeValid = (key, value) => {
  if (key === "valid") {
    return undefined;
  }
  return value;
};

const removeIdsAndTimestamps = (key, value) => {
  if (key === "_id" || key === "updatedAt" || key === "createdAt" || key === "dialectId") {
    return undefined;
  }
  return value;
};

const cloneWithoutTimestamps = (o) => {
  return cloneObject(o, removeTimestamps);
};

const cloneWithoutIdsAndTimestamps = (o) => {
  return cloneObject(o, removeIdsAndTimestamps);
};


const cloneWithoutValidField = (o) => {
  return cloneObject(o, removeValid);
};

 

export default {
	cloneWithoutTimestamps,
	cloneWithoutIdsAndTimestamps,
	 cloneWithoutValidField,
	 cloneObject,
	 insertDialectType
	}

