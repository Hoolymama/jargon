import React, { Component, PropTypes } from "react";
import Datastore from "react-native-local-mongodb";
import * as JDB from "@hoolymama/jdb-utils";
import Meteor from "react-native-meteor";

function WithDatabase(WrappedComponent) {
  return class extends React.Component {
    
    constructor(props) {
      super(props);
      this.state = {};

      
      const collections = ["DialectsDb", "TranscriptionsDb"];

      collections.forEach(collection => {
        this.state[collection] = new Datastore({
          filename: collection,
          autoload: true,
          timestampData: true
        });
      });

      /* collect all the db access functions in dbMethods object
      makes it convenient to pass them all as props using spread */
      this.dbMethods = {
        findClientDialects: this.findClientDialects.bind(this),
        findServerDialects: this.findServerDialects.bind(this),
        removeDialect: this.removeDialect.bind(this),
        saveDialect: this.saveDialect.bind(this),
        transferDialect: this.transferDialect.bind(this),
        findTranscriptions: this.findTranscriptions.bind(this),
        removeTranscription: this.removeTranscription.bind(this),
        saveTranscription: this.saveTranscription.bind(this),
        log: this.logAll.bind(this)
      };
    }
    componentDidMount() {}

    //////////////////////// METHODS ///////////////////////////

    logAll() {
      this.state.DialectsDb.find({}, (errD, resultD) => {
        resultD.forEach(d => {
          this.state.TranscriptionsDb.find({ dialectId: d._id }, (
            errT,
            resultT
          ) => {
            d.transcriptions = resultT;
            const json = JDB.cloneWithoutIdsAndTimestamps(d);
            console.log(json);
          });
        });
      });
    }

    findClientDialects(callback) {
      this.state.DialectsDb.find({}, (err, result) => {
        let dialects = [];
        if (!err) {
          dialects = JDB.insertDialectType(result, "client");
        }
        callback(err, dialects);
      });
    }

    findServerDialects(callback) {
      Meteor.call("getMyDialects", (err, result) => {
        let dialects = [];
        if (!err) {
          dialects = JDB.insertDialectType(result, "server");
        }
        callback(err, dialects);
      });
    }

    removeDialect(dialect, callback) {
      if (dialect.type === "client") {
        this.state.DialectsDb.remove({ _id: dialect._id }, {}, err => {
          if (err) {
            callback(err);
          } else {
            this.state.TranscriptionsDb.remove(
              { dialectId: dialect._id },
              { multi: true },
              callback
            );
          }
        });
      } else {
        /* remove server dialect */
        Meteor.call("removeDialect", dialect._id, callback);
      }
    }

    saveDialect(dialect, callback) {
      if (dialect._id) {
        if (dialect.type === "client") {
          this.state.DialectsDb.update(
            { _id: dialect._id },
            {
              $set: {
                name: dialect.name,
                description: dialect.description
              }
            },
            callback
          );
        } else {
          /* update server dialect */
          console.log("UPDATE ON SERVER");

          Meteor.call("updateDialect", dialect, callback);
        }
      } else {
        /*always save new dialects locally */
        this.state.DialectsDb.insert(dialect, callback);
      }
    }

    saveTranscription(dialect, transcription, callback) {
      if (transcription._id) {
        /*update */
        if (dialect.type === "client") {
          this.state.TranscriptionsDb.update(
            { _id: transcription._id },
            {
              $set: {
                source: transcription.source,
                outcomes: transcription.outcomes
              }
            },
            callback
          );
        } else {
          /* update server dialect */
          Meteor.call("updateTranscription", transcription, callback);
        }
      } else {
        if (dialect.type === "client") {
          transcription.dialectId = dialect._id;
          this.state.TranscriptionsDb.insert(transcription, callback);
        } else {
          Meteor.call(
            "createTranscription",
            dialect._id,
            transcription,
            callback
          );
        }
      }
    }

    // get the dialect and all its transcriptions and create a chunk of JSON

    transferClientToServer(dialect, callback) {
      console.log("transferClientToServer");

      console.log(dialect);
      this.state.DialectsDb.findOne({ _id: dialect._id }, (
        errD,
        resultDialect
      ) => {
        if (errD) {
          callback(errD);
        } else {
          console.log("dialect._id: " + dialect._id);

          this.state.TranscriptionsDb.find({ dialectId: dialect._id }, (
            errT,
            resultTranscriptions
          ) => {
            if (errT) {
              callback(errT);
            } else {
              console.log("resultTranscriptions");

              console.log(resultTranscriptions);
              resultDialect.transcriptions = resultTranscriptions;
              console.log("resultDialect");
              console.log(resultDialect);

              const json = JDB.cloneWithoutIdsAndTimestamps(resultDialect);
              console.log("json");
              console.log(json);

              Meteor.call("createDialect", json, (err, result) => {
                if (!err) {
                  this.removeDialect(dialect, callback);
                } else {
                  callback(err, result);
                }
              });
            }
          });
        }
      });
    }

    transferServerToClient(dialect, callback) {
      Meteor.call("getDialectPayload", dialect._id, (errP, payload) => {
        if (!errP) {
          this.state.DialectsDb.insert(
            {
              name: payload.name,
              description: payload.description
            },
            (errD, newDialect) => {
              if (!errD) {
                payload.transcriptions.forEach(t => {
                  t.dialectId = newDialect._id;
                  this.state.TranscriptionsDb.insert(t);
                });
                this.removeDialect(dialect, callback);
              } else {
                callback(errD);
              }
            }
          );
        } else {
          callback(errP);
        }
      });
    }

    transferDialect(dialect, callback) {
      if (dialect.type === "client") {
        this.transferClientToServer(dialect, callback);
      } else {
        this.transferServerToClient(dialect, callback);
      }
    }

    findTranscriptions(dialect, callback) {
      if (dialect.type === "client") {
        this.state.TranscriptionsDb.find({ dialectId: dialect._id }, callback);
      } else {
        Meteor.call("getTranscriptions", dialect._id, callback);
      }
    }

    removeTranscription(transcription, callback) {
      if (transcription.type === "client") {
        this.state.TranscriptionsDb.remove(
          { _id: transcription._id },
          {},
          callback
        );
      } else {
        Meteor.call("removeTranscription", transcription._id, callback);
      }
    }
    ////////////////////////////////////////////////

    render() {
      return (
        <WrappedComponent {...this.props} {...this.state} db={this.dbMethods} />
      );
    }
  };
}

export default WithDatabase;
