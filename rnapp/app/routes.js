"use strict"

import Dialects from "./components/Dialects"
import Vocabulary from "./components/Vocabulary"
// import Share from "./components/Share"
// import TranslateWeb from "./components/TranslateWeb"
// import TranslateText from "./components/TranslateText"
 import Settings from "./components/Settings"
 
const routes = { 
	dialects: { page: Dialects }, 
	settings: { page: Settings },
  vocabulary: { page: Vocabulary } 
	// share: { page: Share },
	// translateweb: { page: TranslateWeb },
	// translatetext : { page: TranslateText }, 

}

export { routes }


