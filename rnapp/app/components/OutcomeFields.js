import React, { Component, PropTypes } from "react";
import { StyleSheet, View, Text, ScrollView } from "react-native";
import { IconToggle, ActionButton } from "react-native-material-ui";

import MDTextInput from "react-native-md-textinput";
import JTextField from "./JTextField/TextField";

const cloneObject = o => {
  return JSON.parse(JSON.stringify(o));
};

class OutcomeFields extends Component {
  constructor(props) {
    super(props);
    this.handleOutcomeChange = this.handleOutcomeChange.bind(this);
    this.handleDeleteOutcome = this.handleDeleteOutcome.bind(this);
    this.handleFabPress = this.handleFabPress.bind(this);
    this.state = { words: cloneObject(props.words) };
  }

  static propTypes() {
    return {
      words: PropTypes.array.isRequired,
      onChange: PropTypes.func
    };
  }

  handleDeleteOutcome(i) {
    const arr = cloneObject(this.state.words);
    arr.splice(i, 1);
    this.setState({ words: arr });
    this.props.onChange(arr);
  }

  handleOutcomeChange(evt, i) {
    const e = evt.nativeEvent;
    const arr = cloneObject(this.state.words);
    arr[i] = e.text;
    this.setState({ words: arr });
    this.props.onChange(arr);
  }

  handleFabPress() {
    const arr = cloneObject(this.state.words);
    arr.push("");
    this.setState({ words: arr });
    this.props.onChange(arr);
  }

  renderDeleteOutcomeButton(i, num) {
    if (num < 2) {
      return <View width={45}><IconToggle name="close" color="#ddd" /></View>;
    }
    return (
      <View width={45} height={40}>
        <IconToggle
          name="close"
          color="#9E9E9E"
          onPress={evt => {
            this.handleDeleteOutcome(i);
          }}
        />
      </View>
    );
  }

  renderOutcomeItems() {
    const arr = cloneObject(this.state.words);
    const num = arr.length;
    return arr.map((o, i) => {
      const shouldFocus = i === num - 1 && arr[i] === "";

      return (
        <View key={i} style={{ flex: 0, flexDirection: "row" }}>
          <View style={{ flexGrow: 1 }}>

            <JTextField
              height={40}
              labelStyle={{ height: 0 }}
              placeholder="Outcome"
              placeholderTextColor="#9E9E9E"
              highlightColor={this.context.uiTheme.palette.primaryColor}
              value={arr[i]}
              onChange={evt => {
                this.handleOutcomeChange(evt, i);
              }}
              returnKeyType="done"
              textColor="#555"
              autoFocus={shouldFocus}
            />
          </View>
          {this.renderDeleteOutcomeButton(i, num)}
        </View>
      );
    });
  }

  render() {
    const styles = getStyles(this.context.uiTheme);

    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ flex: 0 }}>
          {this.renderOutcomeItems()}
        </ScrollView>
        <ActionButton onPress={this.handleFabPress} />
      </View>
    );
  }
}
OutcomeFields.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default OutcomeFields;

const getStyles = function(theme) {
  return StyleSheet.create({});
};
