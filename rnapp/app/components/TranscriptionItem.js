import React, { Component, PropTypes } from "react";
import { Icon, Avatar, IconToggle } from "react-native-material-ui";
import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";
import TranscriptionInputModal from "./TranscriptionInputModal";
import DotsMenu from "./DotsMenu";

class TranscriptionItem extends Component {
  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleShowDetail = this.handleShowDetail.bind(this);

    this.state = {
      menuItems: [
              {
          label: "Edit",
          disabled: false,
          callback: this.handleEdit
        },
        {
          label: "Delete",
          disabled: false,
          callback: this.handleDelete
        },
        {
          label: "Show",
          disabled: false,
          callback: this.handleShowDetail
        }
      ]
    };
  }

  static propTypes() {
    return {
      navigator: PropTypes.object.isRequired,
      transcription: PropTypes.object.isRequired,
      onDelete: PropTypes.func,
      onEdit: PropTypes.func,
      onShow: PropTypes.func
    };
  }

  handleEdit() {
    if (this.props.onEdit) {
      this.props.onEdit(this.props.transcription);
    }
  }

  handleDelete() {
    if (this.props.onDelete) {
      this.props.onDelete(this.props.transcription);
    }
  }

  handleShowDetail() {
    if (this.props.onShow) {
      this.props.onShow(this.props.transcription);
    }
  }

  renderOutcomes(styles) {
    return this.props.transcription.outcomes.map((o, i) => {
      return (
        <View style={styles.outcome} key={i}>
          <Text style={styles.outcomeText}>{o.text}</Text>
        </View>
      );
    });
  }

  renderCenterElement(styles) {
    return (
      <View style={styles.centerItemContainer}>
        <TouchableOpacity
          onPress={this.handleEdit}
          onLongPress={this.handleDelete}
        >
          <View style={styles.contentContainer}>
            <View style={styles.nameContainer}>
              <Text style={styles.nameText}>
                {this.props.transcription.source.text}
              </Text>
            </View>
            <View style={styles.outcomes}>
              {this.renderOutcomes(styles)}
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  renderRightElement(styles) {
    return (
      <View style={styles.rightItemContainer}>
        <DotsMenu
          style={{ icon: styles.rightIcon }}
          items={this.state.menuItems}
        />
      </View>
    );
  }

  render() {
    const styles = getStyles(this.context.uiTheme);
    return (
      <View style={styles.listItemContainer}>
        {this.renderCenterElement(styles)}
        {this.renderRightElement(styles)}
      </View>
    );
  }
}

TranscriptionItem.contextTypes = { uiTheme: PropTypes.object.isRequired };

const getStyles = function(theme) {
  return StyleSheet.create({
    listItemContainer: {
      padding: 16,
      paddingVertical: 8,
      flexDirection: "row",
      borderBottomWidth: 1,
      borderColor: "rgba(0, 0, 0, 0.05)"
    },
    centerItemContainer: { width: 160, flexGrow: 1 },
    rightItemContainer: { width: 42 },
    contentContainer: { flex: 0, flexDirection: "column", paddingRight: 8 },
    nameText: {
      ...theme.typography.subheading,
      color: "#000",
      paddingLeft: 4
    },

    outcomes: {
      flex: 0,
      flexDirection: "row",
      flexWrap: "wrap",
      alignItems: "flex-start"
    },
    outcome: {
      marginTop: 8,
      backgroundColor: "#eee",
      paddingHorizontal: 8,
      marginHorizontal: 4,
      borderRadius: 2,
      borderWidth: 1,
      borderColor: "#ddd"
    },
    outcomeText: { fontSize: 12, color: "#444" }
  });
};

export default TranscriptionItem;
