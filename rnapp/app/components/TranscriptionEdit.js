import React, { Component, PropTypes } from "react";
import {
  TouchableWithoutFeedback,
  StyleSheet,
  View,
  Text,
  TextInput,
  Platform,
  ScrollView
} from "react-native";
import {
  Toolbar,
  Icon,
  Button,
  IconToggle,
  ActionButton
} from "react-native-material-ui";

import VToolbar from "./VToolbar";
import Toast from "@remobile/react-native-toast";

import MDTextInput from "react-native-md-textinput";
import OutcomeFields from "./OutcomeFields";

const validate = field => {
  field.valid = field.text.length > 0;
  return field;
};

const removeTimestamps = (key, value) => {
  if (key === "updatedAt" || key === "createdAt") {
    return undefined;
  }
  return value;
};
const removeValid = (key, value) => {
  if (key === "valid") {
    return undefined;
  }
  return value;
};

const transcriptionValid = transcription => {
  const invalid = transcription.outcomes.filter(o => {
    return o.valid === false;
  });
  return invalid.length === 0 && transcription.source.valid;
};

class TranscriptionEdit extends Component {
  constructor(props) {
    super(props);

    this.handleSourceChange = this.handleSourceChange.bind(this);
    this.handleOutcomesChange = this.handleOutcomesChange.bind(this);
    this.handleSendButtonPress = this.handleSendButtonPress.bind(this);

    this.state = {};
    this.state.transcription = this.cloneTranscription(props.transcription);

    this.state.isNew = !props.transcription._id;
    this.state.title = this.state.isNew
      ? "New Transcription"
      : "Edit Transcription";
  }

  static propTypes() {
    return {
      onClose: PropTypes.func,
      onSave: PropTypes.func,
      transcription: PropTypes.object.isRequired
    };
  }

  cloneTranscription(transcription) {
    const t = JSON.parse(JSON.stringify(transcription, removeTimestamps));
    t.source = validate(t.source);
    t.outcomes = t.outcomes.map(outcome => {
      return validate(outcome);
    });
    return t;
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      transcription: this.cloneTranscription(nextProps.transcription)
    });
  }

  handleSourceChange(evt) {
    const e = evt.nativeEvent;
    const t = this.state.transcription;
    t.source.text = e.text;
    t.source = validate(t.source);
    this.setState({ transcription: t });
  }

 
  handleOutcomesChange(arr) {
     const t = this.state.transcription;
     t.outcomes  = arr.map(o => {
      return validate({text: o})
    })
    this.setState({ transcription: t });
  }

  handleSendButtonPress() {
    const t = this.state.transcription;
    if (transcriptionValid(t)) {
      const result = JSON.parse(JSON.stringify(t, removeValid));
      this.props.onSave(result);
    } else {
      Toast.showShortTop("Invalid name or outcome");
    }
  }
 

  renderSendButton(styles) {
    const t = this.state.transcription;
    if (transcriptionValid(t)) {
      return (
        <Button
          accent
          text="Save"
          onPress={this.handleSendButtonPress}
          style={{ text: styles.toolbarButtonText }}
        />
      );
    } else {
      return (
        <Button
          accent
          disabled
          text="Save"
          style={{ text: styles.disabledToolbarButtonText }}
        />
      );
    }
  }

  renderSource(styles) {
    return (
      <View style={{ flex: 0 }}>
        <MDTextInput
          height={50}
          label="Source"
          highlightColor={this.context.uiTheme.palette.primaryColor}
          value={this.state.transcription.source.text}
          onChange={this.handleSourceChange}
          returnKeyType="done"
          textColor="#555"
          duration={100}
          autoFocus={this.state.isNew}
        />
      </View>
    );
  }
 
  outcomesText(){
    return  this.state.transcription.outcomes.map(o => o.text)
  }

  render() {
    const styles = getStyles(this.context.uiTheme);

    return (
      <View style={styles.modalContainer}>
        <VToolbar
          leftElement="close"
          onLeftElementPress={this.props.onClose}
          centerElement={this.state.title}
          style={{
            centerElementContainer: {
              flexDirection: "row",
              justifyContent: "center"
            }
          }}
          rightElement={this.renderSendButton(styles)}
        />
        
        <View style={styles.contentContainer}>
          {this.renderSource(styles)}
          <View>
            <Text style={{ marginTop:16, marginBottom:8,color: '#9E9E9E', fontSize: 12 }}>
              Outcomes
            </Text>
          </View>
         <OutcomeFields 
          words={this.outcomesText()} 
          onChange={this.handleOutcomesChange}/>
        </View>

     
      </View>
    );
  }
}

TranscriptionEdit.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default TranscriptionEdit;

const getStyles = function(theme) {
  return StyleSheet.create({
    toolbarButtonText: { color: "white" },
    disabledToolbarButtonText: { color: "white", opacity: 0.5 },
    modalContainer: { flex: 1, backgroundColor: "white" },
    contentContainer: { flex: 1, margin: 16, marginTop: 16 }
  });
};
