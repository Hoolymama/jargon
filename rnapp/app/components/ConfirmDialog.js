import React, {Component, PropTypes} from "react";
import {StyleSheet, View, Text} from "react-native";

import FloatingModal from "./FloatingModal";

class ConfirmDialog extends Component {
  constructor(props) {
    super(props);
    this.handleNo = this.handleNo.bind(this);
    this.handleYes = this.handleYes.bind(this);
  }

  static propTypes() {
    return {
      animationType: PropTypes.string,
      onNo: PropTypes.func,
      onYes: PropTypes.func,
      visible: PropTypes.bool,
      title: PropTypes.string.isRequired,
      message: PropTypes.string.isRequired,
      yesButtonText: PropTypes.string,
      noButtonText: PropTypes.string,
      data: PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.number,
        React.PropTypes.bool,
        React.PropTypes.object
      ])
    };
  }

  handleNo() {
    if (this.props.onNo) {
      this.props.onNo();
    }
  }

  handleYes() {
    if (this.props.onYes) {
      this.props.onYes();
    }
  }

  render() {
    const styles = getStyles(this.props, this.context.uiTheme);

    return (
      <FloatingModal showDividers={false} onSubmit={this.handleYes} onClose={
        this.handleNo
      } visible={this.props.visible} title={
        this.props.title
      } valid="true" submitButtonText={
        this.props.yesButtonText
      } cancelButtonText={this.props.noButtonText}>
        <View>
          <Text style={styles.message}>
            {this.props.message}
          </Text>
        </View>
      </FloatingModal>
    );
  }
}

const getStyles = function(props, theme) {
  return StyleSheet.create({
    message: { marginBottom: 24, lineHeight: 24, fontSize: 16 }
  });
};
ConfirmDialog.contextTypes = { uiTheme: PropTypes.object.isRequired };

ConfirmDialog.defaultProps = {
  visible: false,
  yesButtonText: "Yes",
  noButtonText: "No"
};

export default ConfirmDialog