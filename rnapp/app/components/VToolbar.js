import React, { Component, PropTypes } from "react";

import { StatusBar, Platform, View } from "react-native";

import { Toolbar } from "react-native-material-ui";

class VToolbar extends Component {
  renderStatusBarPadIOS() {
    if (Platform.OS === "ios") {
      return <View style={
        {
          height: 20,
          backgroundColor: this.context.uiTheme.palette.secondaryColor
        }
      } />;
    }
  }

  render() {
    return (
      <View>
        <StatusBar barStyle="light-content" />
        {this.renderStatusBarPadIOS()}
        <Toolbar {...this.props} />
      </View>
    );
  }
}

VToolbar.contextTypes = { uiTheme: PropTypes.object.isRequired };

export default VToolbar;
// style={{centerElementContainer: {
// 		flexDirection:"row",
// 		justifyContent:"center"
// 	}}}
// 	{{centerElementContainer: {
// 	flexDirection:"row",
// 	justifyContent:"center"
// }}}
