import React, {Component, PropTypes} from "react";

import {Text, View, StyleSheet, Modal, Dimensions} from "react-native";

import {Toolbar, Button} from "react-native-material-ui";

import {ImageCrop} from "react-native-image-cropper";

const { width, height } = Dimensions.get("window");

const CROP_WINDOW_WIDTH = Math.min(Math.min(width, 512), height) - 60;

import ImageResizer from "react-native-image-resizer";

import VToolbar from "./VToolbar";

const getStyles = function(theme) {
  return StyleSheet.create({
    toolbarButtonText: { color: "white" },
    disabledToolbarButtonText: { color: "white", opacity: 0.5 },
    modalContainer: {
      flex: 1,
      backgroundColor: "#222",
      borderWidth: 1,
      borderColor: "blue"
    },
    contentContainer: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center"
    },
    heading: {
      fontSize: 16,
      marginLeft: 6,
      color: theme.palette.disabledTextColor
    },
    headingBox: { height: 18 },
    textInput: { flex: 1, flexGrow: 1, fontSize: 30, color: "#555" },
    placeholder: {
      marginTop: 8,
      marginLeft: 4,
      flex: 1,
      flexGrow: 1,
      fontSize: 30,
      color: theme.palette.disabledTextColor
    }
  });
};

class AvatarCropperModal extends Component {
  constructor(props) {
    super(props);

    this.handleClose = this.handleClose.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }

  doNothing() {}

  static propTypes() {
    return {
      onClose: PropTypes.func,
      onSave: PropTypes.func,
      uri: PropTypes.string
    };
  }

  handleClose() {
    this.props.onClose();
  }

  handleSave() {
    this.refs.cropper.crop().then(result => {
      ImageResizer
        .createResizedImage(result, 128, 128, "JPEG", 100, 0)
        .then(smallImageUrl => {
          // resizeImageUri is the URI of the new image that can now be displayed, uploaded...
          this.props.onSave(smallImageUrl);
          // console.log(resizedImageUri)
        })
        .catch(err => {
          console.log("Error with image resizer on crop");
        });
      // this.props.onSave(result)
    });
  }

  tmpPath() {
    return this.props.uri.replace("file:", "") + "_cropped.jpg";
  }

  render() {
    const styles = getStyles(this.context.uiTheme);

    return (
      <Modal animationType={"none"} transparent visible onRequestClose={
        this.doNothing
      }>
        <View style={styles.modalContainer}>
          <VToolbar leftElement="close" onLeftElementPress={
            this.handleClose
          } centerElement="Photo Cropper" style={
            {
              centerElementContainer: {
                flexDirection: "row",
                justifyContent: "center"
              }
            }
          } rightElement={
            <Button accent text="Save" onPress={this.handleSave} style={
              { text: styles.toolbarButtonText }
            } />
          } />
          <View style={styles.contentContainer}>
            <ImageCrop ref={"cropper"} image={this.props.uri} cropHeight={
              CROP_WINDOW_WIDTH
            } cropWidth={CROP_WINDOW_WIDTH} zoomFactor={1} maxZoom={
              80
            } minZoom={20} type="jpg" panToMove // format='file'
            // filePath={this.tmpPath()}
            pinchToZoom />
          </View>
        </View>
      </Modal>
    );
  }
}

AvatarCropperModal.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default AvatarCropperModal
// <ImageCrop 
// 		ref={"cropper"}
// 		image={this.props.uri}
// 		cropHeight={256}
// 		cropWidth={256}
// 		zoomFactor={1}
// 		maxZoom={80}
// 		minZoom={20}
//     type="jpg"
// 		panToMove
// 		pinchToZoom
// 		/>