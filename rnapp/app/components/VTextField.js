import React, { Component, PropTypes } from "react";

import { StyleSheet, TextInput } from "react-native";

const getStyles = function(theme) {
  return StyleSheet.create({
    textfieldView: {
      flex: 0,
      marginBottom: 16,
      backgroundColor: "#FFFFFF",
      fontSize: 14,
      paddingLeft: 10,
      borderRadius: 2,
      height: 35,
      backgroundColor: "#eee"
    }
  });
};

class VTextField extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes() {
    return {
      onTextChange: PropTypes.func.isRequired,
      placeholder: PropTypes.string,
      autoFocus: PropTypes.bool,
      type: PropTypes.string,
      value: PropTypes.string
    };
  }

  static defaultProps = { type: "default", autoFocus: false };

  render() {
    const styles = getStyles(this.context.uiTheme);

    return (
      <TextInput
        underlineColorAndroid="transparent"
        style={styles.textfieldView}
        secureTextEntry={this.props.type === "password"}
        onChangeText={this.props.onTextChange}
        value={this.props.value}
        keyboardType={this.props.type === "email" ? "email-address" : "default"}
        placeholder={this.props.placeholder}
        autoFocus={this.props.autoFocus}
      />
    );
  }
}

VTextField.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default VTextField;
