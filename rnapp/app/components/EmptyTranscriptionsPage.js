import React, { Component, PropTypes } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { IconToggle } from "react-native-material-ui";

const getStyles = function(theme) {
  return StyleSheet.create({
    page: { flex: 1, padding: 20 },
    title: {
      marginBottom: 10,
      textAlign: "center",
      fontSize: 20,
      fontWeight: "bold",
      color: "#888"
    },
    heading: {
      marginTop: 10,
      marginBottom: 20,
      textAlign: "center",
      fontSize: 16,
      fontWeight: "bold",
      color: "#888"
    },
    body: { marginTop: 0, fontSize: 12, fontWeight: "normal" },
    li: { marginTop: 5, marginLeft: 10, fontSize: 12, fontWeight: "normal" }
  });
};

class EmptyTranscriptionsPage extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes() {
    return { loading: PropTypes.bool.isRequired };
  }

  render() {
    const styles = getStyles(this.context.uiTheme);

    if (this.props.loading) {
      return (
        <View style={styles.page}>

          <Text style={styles.title}>Loading...</Text>

        </View>
      );
    }

    return (
      <View style={styles.page}>
        <Text style={styles.title}>No transcriptions</Text>
        <Text style={styles.heading}>
          Use the floating button below to create a new transcription.
        </Text>
      </View>
    );
  }
}
EmptyTranscriptionsPage.contextTypes = { uiTheme: PropTypes.object.isRequired };

export default EmptyTranscriptionsPage;
