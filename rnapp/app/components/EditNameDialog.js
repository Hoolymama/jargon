import React, {Component, PropTypes} from "react";
import {StyleSheet, View, Text, TextInput, Platform} from "react-native";
import {Button} from "react-native-material-ui";

import FloatingModal from "./FloatingModal";

class EditNameDialog extends Component {
  constructor(props) {
    super(props);

    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);

    this.state = { valid: false, text: props.initialText, error: "" };
  }

  static propTypes() {
    return {
      onClose: PropTypes.func,
      onSubmit: PropTypes.func,
      visible: PropTypes.bool,
      title: PropTypes.string.isRequired,
      initialText: PropTypes.string
    };
  }

  static defaultProps = { visible: false, initialText: "" };

  handleClose() {
    if (this.props.onClose) {
      this.props.onClose();
    }
  }

  handleSubmit() {
    if (this.props.onSubmit) {
      this.props.onSubmit(this.state.text);
    }
  }

  onChangeText(text) {
    const valid = text.length > 3;
    this.setState({ text, valid });
  }

  render() {
    const styles = getStyles(this.props, this.context.uiTheme);

    return (
      <FloatingModal showDividers={false} onSubmit={this.handleSubmit} onClose={
        this.handleClose
      } visible={this.props.visible} title={this.props.title} valid={
        this.state.valid
      } submitButtonText="save" cancelButtonText="cancel">
        <View style={
          {
            flex: 0,
            marginBottom: 24,
            borderBottomWidth: Platform.OS === "ios" ? 2 : 0,
            borderColor: this.context.uiTheme.palette.primaryColor
          }
        }>
          <TextInput returnKeyType="done" placeholderTextColor="rgba(0,0,0,0.38)" textColor="#000" placeholder="Name" underlineColorAndroid={
            this.context.uiTheme.palette.primaryColor
          } multiline={false} onChangeText={this.onChangeText} style={
            styles.textInput
          } value={this.state.text} />
        </View>
      </FloatingModal>
    );
  }
}

const getStyles = function(props, theme) {
  return StyleSheet.create({
    textInput: {
      height: Platform.OS === "ios" ? 30 : 40,
      fontSize: 18,
      color: "#555"
    }
  });
};

EditNameDialog.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default EditNameDialog