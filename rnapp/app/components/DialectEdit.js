import React, { Component, PropTypes } from "react";
import {
  TouchableWithoutFeedback,
  StyleSheet,
  View,
  Text,
  TextInput,
  Platform
} from "react-native";
import { Toolbar, Icon, Button } from "react-native-material-ui";

import VToolbar from "./VToolbar";
import Toast from "@remobile/react-native-toast";
import MDTextInput from "react-native-md-textinput";
const getStyles = function(theme) {
  return StyleSheet.create({
    toolbarButtonText: { color: "white" },
    disabledToolbarButtonText: { color: "white", opacity: 0.5 },
    modalContainer: { flex: 1, backgroundColor: "white" },
    contentContainer: { flex: 1, margin: 16, marginTop: 30 },
    textInput: {
      fontSize: 14,
      color: "#555",
      borderBottomWidth: Platform.OS === "ios" ? 2 : 0,
      borderBottomColor: theme.palette.primaryColor
    },
    dummyTextInput: {
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      opacity: 0,
      color: "blue",
      paddingLeft: 4,
      paddingRight: 4,
      fontSize: 14
    },
    placeholder: {
      marginTop: 8,
      marginLeft: 4,
      flex: 1,
      flexGrow: 1,
      fontSize: 14,
      color: theme.palette.disabledTextColor
    },
    iconContainer: {
      width: 44,
      height: 44,
      borderRadius: 22,
      flex: 0,
      justifyContent: "center",
      alignItems: "center",
      elevation: 2,
      backgroundColor: theme.palette.primaryColor
    },
    icon: {
      width: 44,
      height: 44,
      borderRadius: 22,
      flex: 0,
      justifyContent: "center",
      alignItems: "center",
      elevation: 2,
      backgroundColor: theme.palette.primaryColor
    },
    buttonContainer: { position: "absolute", bottom: 10, right: 16 }
  });
};

const removeTimestamps = (key, value) => {
  if (key === "updatedAt" || key === "createdAt") {
    return undefined;
  }
  return value;
};

const isValid = dialect => {
  return (dialect.name &&
      dialect.name.length > 0 &&
      dialect.description &&
      dialect.description.length > 0) ? true : false;
};

class DialectEdit extends Component {
  constructor(props) {
    super(props);

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleSendButtonPress = this.handleSendButtonPress.bind(this);

    this.state = {
      dialect: this.cloneDialect(props.dialect),
      valid: isValid(props.dialect)
    };

    this.state.isNew = !props.dialect._id;

    this.state.title = this.state.isNew ? "New Dialect" : "Edit Dialect";
  }

  static propTypes() {
    return {
      onClose: PropTypes.func,
      onSave: PropTypes.func,
      dialect: PropTypes.object.isRequired
    };
  }

  cloneDialect(dialect) {
    const d = JSON.parse(JSON.stringify(dialect, removeTimestamps));
    return d;
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      dialect: this.cloneDialect(nextProps.dialect),
      valid: isValid(nextProps.dialect)
    });
  }

  handleNameChange(evt) {
    const dialect = this.state.dialect;
    dialect.name = evt.nativeEvent.text;
    this.setState({
      dialect: dialect,
      valid: isValid(dialect)
    });
  }

  handleDescriptionChange(evt) {
    const dialect = this.state.dialect;
    dialect.description = evt.nativeEvent.text;

    const valid =  isValid(dialect)
     // console.log("handleDescriptionChange")
     //  console.log(dialect)
     // console.log(valid)

    this.setState({
      dialect: dialect,
      valid: isValid(dialect)
    });
  }

  handleSendButtonPress() {
    if (this.state.valid) {
      const result = JSON.parse(JSON.stringify(this.state.dialect));
      this.props.onSave(result);
    } else {
      Toast.showShortTop("Invalid name or description");
    }
  }

  renderSendButton(styles) {
    const style= this.state.valid ? 
      {text: styles.toolbarButtonText} :
      {text: styles.disabledToolbarButtonText }

    return (
      <Button
        accent
        text="Save"
        disabled={!this.state.valid}
        onPress={this.handleSendButtonPress}
        style={style}
      />
    );
 
    }

 

  renderTextInput(styles) {
    return (
      <View style={{ flex: 0 }}>
         <MDTextInput
          height={50}
          label="Name"
          highlightColor={this.context.uiTheme.palette.primaryColor}
          value={this.state.dialect.name}
          onChange={this.handleNameChange}
          returnKeyType="done"
          textColor="#555"
          duration={200}
          autoFocus
        />

         <MDTextInput
          height={50}
          label="Description"
          highlightColor={this.context.uiTheme.palette.primaryColor}
          value={this.state.dialect.description}
          onChange={this.handleDescriptionChange}
          returnKeyType="done"
          textColor="#555"
          duration={200}
  
        />


      </View>
    );
  }

  render() {
    const styles = getStyles(this.context.uiTheme);

    return (
      <View style={styles.modalContainer}>
        <VToolbar
          leftElement="close"
          onLeftElementPress={this.props.onClose}
          centerElement={this.state.title}
          style={{
            centerElementContainer: {
              flexDirection: "row",
              justifyContent: "center"
            }
          }}
          rightElement={this.renderSendButton(styles)}
        />
        <View style={styles.contentContainer}>
          {this.renderTextInput(styles)}
        </View>

      </View>
    );
  }
}
DialectEdit.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default DialectEdit;
// {this.renderMicButton(styles)}
