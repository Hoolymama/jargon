"use strict";
// import Meteor from "react-native-meteor";
import React, { Component, PropTypes } from "react";
import { View, StyleSheet, AppState, Platform } from "react-native";
import { Navigator  } from "react-native-deprecated-custom-components";

import { routes } from "../routes";


class Navigation extends Component {



  constructor(props) {
    super(props);
    this.renderScene = this.renderScene.bind(this);
    this.configureScene = this.configureScene.bind(this);
 
  }


  // componentDidMount() {
  //   console.log("NAV MOUNT");
  // }
  componentWillUnmount() {}


  static navigationOptions = {
    title: 'Welcome',
  };

  static propTypes() {
    return {
      currentUser: PropTypes.object.isRequired,
      status:  PropTypes.object.isRequired,
      onLogout: PropTypes.func.isRequired 
    };
  }


  configureScene(route, routeStack) {
    return Navigator.SceneConfigs.FadeAndroid;
  }

  renderScene(route, navigator) {
    return (
      <route.page
      {...this.props}
      {...route.passProps}
      navigator={navigator}
      />
      );
  }
 // {...route.passProps}
  render() {
    return (
      <View style={{ flex: 1 }}>
      <Navigator
      initialRoute={routes.dialects}
      renderScene={this.renderScene}
      configureScene={this.configureScene}
      />
      </View>
      );
  }
}

export default Navigation;
