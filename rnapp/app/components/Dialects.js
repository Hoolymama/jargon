import React, { Component, PropTypes } from "react";
import Meteor from "react-native-meteor";

import { View, Platform, BackHandler, Text, StyleSheet } from "react-native";
import { ActionButton } from "react-native-material-ui";
import { routes } from "../routes";
import DialectsToolbar from "./DialectsToolbar";

import Toast from "@remobile/react-native-toast";
import DialectsListView from "./DialectsListView";
import DialectInputModal from "./DialectInputModal";
import ConfirmDialog from "./ConfirmDialog";
import { TabViewAnimated, TabBar } from "react-native-tab-view";

class Dialects extends Component {
  constructor(props) {
    super(props);

    // console.log("props.findClientDialects");
    // console.log(props.findClientDialects);

    this.navigateToSettings = this.navigateToSettings.bind(this);

    this.handleShowDelete = this.handleShowDelete.bind(this);
    this.handleCancelDelete = this.handleCancelDelete.bind(this);
    this.handleDoDelete = this.handleDoDelete.bind(this);
    this.deleteDialectMessage = this.deleteDialectMessage.bind(this);

    this.handleShowTransfer = this.handleShowTransfer.bind(this);
    this.handleCancelTransfer = this.handleCancelTransfer.bind(this);
    this.handleDoTransfer = this.handleDoTransfer.bind(this);
    this.transferDialectMessage = this.transferDialectMessage.bind(this);

    this.handleShowEdit = this.handleShowEdit.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleCloseInputModal = this.handleCloseInputModal.bind(this);

    this.handleFabPress = this.handleFabPress.bind(this);

    // this.handleFetchDialectsMine = this.handleFetchDialectsMine.bind(this);
    this.fetchDialectsMine = this.fetchDialectsMine.bind(this);

    this.handleChangeTab = this.handleChangeTab.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderTabScene = this.renderTabScene.bind(this);


    this.handleLogAll = this.handleLogAll.bind(this);


    this.state = {
      deleteDialogVisible: false,
      inputVisible: false,
      dialectToDelete: null,
      dialectToTransfer: null,
      fetchingDialects: false,
      initialDialect: {},
      dialectsClient: [],
      dialectsServer: [],
      dialectsPublic: [],

      menuItems: [
        { label: "Log All Local", disabled: false, callback: this.handleLogAll }
      ],

      tabs: {
        index: 0,
        routes: [
          { key: "1", title: "Mine" },
          { key: "2", title: "Shared" },
          { key: "3", title: "Public" }
        ]
      }
    };
  }

  handleLogAll(){
    this.props.db.log();
  }

  ///////////////////
  componentDidMount() {
    if (Platform.OS === "android") {
      BackHandler.addEventListener("hardwareBackPress", () => {
        BackHandler.exitApp(0);
        return true;
      });
    }
    this.fetchDialectsMine();
  }

  componentWillUnmount() {
    if (Platform.OS === "android") {
      BackHandler.removeEventListener("hardwareBackPress");
    }
  }
  ///////////////////

  onlineMode() {
    return this.props.status.connected &&
      this.props.currentUser.serviceType !== "offline";
  }

  static propTypes() {
    return {
      currentUser: PropTypes.object.isRequired,
      navigator: PropTypes.object.isRequired,
      db: React.PropTypes.objectOf(React.PropTypes.func).isRequired

      // dbFindClientDialects: PropTypes.func.isRequired,
      // dbFindServerDialects: PropTypes.func.isRequired,
      // dbRemoveDialect: PropTypes.func.isRequired,
      // dbSaveDialect: PropTypes.func.isRequired,
      // dbTransferDialect: PropTypes.func.isRequired, 
      // dbLog: PropTypes.func.isRequired,
     
    };
  }

  navigateToSettings() {
    this.props.navigator.push({ ...routes.settings });
  }

  fetchDialectsMine() {
    this.props.db.findClientDialects((err, result) => {
      // console.log("db.findClientDialects");
      // console.log(result);
      this.setState({ dialectsClient: result });
    });

    this.props.db.findServerDialects((err, result) => {
      // console.log("db.findServerDialects");
      // console.log(result);

      this.setState({ dialectsServer: result });
    });
  }

  gatherDialectsMine() {
    return [...this.state.dialectsClient, ...this.state.dialectsServer];
  }

  ///////////////////
  handleCancelDelete() {
    this.setState({ deleteDialectId: null, deleteDialogVisible: false });
  }

  handleDoDelete() {
    const d = this.state.dialectToDelete;
    this.props.db.removeDialect(d, err => {
      if (!err) {
        Toast.showShortTop("Removed dialect: " + d.name);
      } else {
        console.log(err);
        Toast.showShortTop("Error - see log");
      }
      this.setState({ dialectToDelete: null, deleteDialogVisible: false });
      this.fetchDialectsMine();
    });
  }

  handleShowDelete(dialect) {
    this.setState({
      dialectToDelete: dialect,
      deleteDialogVisible: true
    });
  }

  deleteDialectMessage() {
    const dialectName = this.state.dialectToDelete &&
      this.state.dialectToDelete.name;
    return `Delete \"${dialectName}\"\n\nAre you sure?`;
  }
  ///////////////////

  ///////////////////
  handleShowTransfer(dialect) {
    console.log("handleShowTransfer");
    console.log(dialect);

    this.setState({
      dialectToTransfer: dialect,
      transferDialogVisible: true
    });
  }

  transferDialectMessage() {
    if (this.state.dialectToTransfer) {
      const dialectName = this.state.dialectToTransfer.name;
      const destination = this.state.dialectToTransfer.type === "client"
        ? "server"
        : "phone";
      return `Transfer \"${dialectName}\" to ${destination}\n\nAre you sure?`;
    }
    return "Something is not right";
  }

  handleCancelTransfer() {
    this.setState({ transferDialectId: null, transferDialogVisible: false });
  }

  handleDoTransfer() {
    console.log("this.state.dialectToTransfer");
    const d = this.state.dialectToTransfer;
    console.log(this.state.dialectToTransfer);

    this.props.db.transferDialect(this.state.dialectToTransfer, (
      err,
      result
    ) => {
      if (err) {
        console.log(err);
        Toast.showShortTop("error occurred - see log");
      } else {
        console.log(result);
        Toast.showShortTop("Dialect trnsferred");
      }
      this.setState({ dialectToTransfer: null, transferDialogVisible: false });
      this.fetchDialectsMine();
    });
  }

  ///////////////////

  ////////////////////////
  handleSave(dialect) {
    console.log("handleSave");
     console.log(dialect);

    this.props.db.saveDialect(dialect, err => {
      if (!err) {
        this.fetchDialectsMine();
      }
    });
    this.setState({ inputVisible: false });
  }

  handleShowEdit(dialect) {
    this.setState({
      inputVisible: true,
      initialDialect: dialect
    });
  }
  handleCloseInputModal() {
    this.setState({ inputVisible: false });
  }
  ////////////////////////

  handleFabPress() {
    this.setState({
      inputVisible: true,
      initialDialect: {
        name: "",
        description: ""
      }
    });
  }

  ////////////////// TABS
  handleChangeTab(index) {
    const tabs = JSON.parse(JSON.stringify(this.state.tabs));
    tabs.index = index;
    this.setState({ tabs });
  }
  renderHeader(props) {
    const styles = getStyles(this.context.uiTheme);
    return (
      <TabBar
        {...props}
        style={styles.tabbar}
        labelStyle={styles.tablabel}
        indicatorStyle={styles.indicator}
      />
    );
  }

  renderTabScene({ route: route }) {
    switch (route.key) {
      case "1":
        return (
          <DialectsListView
            navigator={this.props.navigator}
            db={this.props.db}
            dialects={this.gatherDialectsMine()}
            onDelete={this.handleShowDelete}
            onEdit={this.handleShowEdit}
            onTransfer={this.handleShowTransfer}
            onRefresh={this.fetchDialectsMine}
            refreshing={this.state.fetchingDialects}
          />
        );
      case "2":
        return <View style={{ flex: 1 }}><Text>SHARED</Text></View>;
      default:
        return <View style={{ flex: 1 }}><Text>PUBLIC</Text></View>;
    }
  }

  //onTransfer={this.onlineMode() ? this.handleShowTransfer : null}

  //////////////////

  render() {
        const styles = getStyles(this.context.uiTheme);


    const self = this;
    const dialectName = this.state.dialectToDelete &&
      this.state.dialectToDelete.name;

    return (
      <View style={{ flex: 1 }}>
        <DialectsToolbar
          onSettingsButton={this.navigateToSettings}
          currentUser={this.props.currentUser}
          menuItems={this.state.menuItems}
        />

        <TabViewAnimated
          style={{ flex: 1 }}
          navigationState={this.state.tabs}
          renderScene={this.renderTabScene}
          renderHeader={this.renderHeader}
          onRequestChangeTab={this.handleChangeTab}
        />
        <ActionButton onPress={this.handleFabPress} />

        <DialectInputModal
          visible={this.state.inputVisible}
          onSave={this.handleSave}
          onClose={this.handleCloseInputModal}
          dialect={this.state.initialDialect}
        />

        <ConfirmDialog
          onNo={this.handleCancelDelete}
          onYes={this.handleDoDelete}
          visible={this.state.deleteDialogVisible}
          title="Delete Dialect"
          message={this.deleteDialectMessage()}
          yesButtonText="Delete"
          noButtonText="Cancel"
        />

        <ConfirmDialog
          onNo={this.handleCancelTransfer}
          onYes={this.handleDoTransfer}
          visible={this.state.transferDialogVisible}
          title="Transfer Dialect"
          message={this.transferDialectMessage()}
          yesButtonText="Transfer"
          noButtonText="Cancel"
        />

      </View>
    );
  }
}

Dialects.contextTypes = { uiTheme: PropTypes.object.isRequired };

export default  Dialects;

const getStyles = function(theme) {
  return StyleSheet.create({
    tabbar: {
      backgroundColor: theme.palette.secondaryColor
    },
    tablabel: {
      color: "#fff",
      fontWeight: "600"
    },
    indicator: {
      backgroundColor: theme.palette.accentColor,
      height: 5
    }
  });
};
