import React, { Component, PropTypes } from "react";

import { Icon, Avatar } from "react-native-material-ui";

import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";

import { routes } from "../routes";
import DotsMenu from "./DotsMenu";

class DialectItem extends Component {
  constructor(props) {
    super(props);
    this.navigateToVocabulary = this.navigateToVocabulary.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleTransfer = this.handleTransfer.bind(this);

    this.state ={menuItems: this.generateMenuItems(props)};

     
  }

  generateMenuItems(props){
    const items = [
        {
          label: "Edit",
          disabled: false,
          callback: this.handleEdit
        },
        {
          label: "Delete",
          disabled: false,
          callback: this.handleDelete
        },
        {
          label: "Vocabulary",
          disabled: false,
          callback: this.navigateToVocabulary
        },
      ]
      if (props.onTransfer) {
        const msg = (props.dialect.type === "client") ? "Upload" : "Download"
        items.push({
          label: msg,
          disabled: false,
          callback: this.handleTransfer
        })
      }
      return items;
        
  }


  componentWillReceiveProps(nextProps) {
    this.setState({menuItems: this.generateMenuItems(nextProps)} );
  }

  static propTypes() {
    return {
      navigator: PropTypes.object.isRequired,
      db: React.PropTypes.objectOf(React.PropTypes.func).isRequired,

      dialect: PropTypes.object.isRequired,
      onDelete: PropTypes.func,
      onEdit: PropTypes.func,
      onTransfer: PropTypes.func
    };
  }

  navigateToVocabulary() {
    // console.log("navigateToVocabulary");
    // console.log(this.props.dialect);
       
    this.props.navigator.push({
      ...routes.vocabulary,
      passProps: {
        dialect: this.props.dialect,
        db:  this.props.db
      }
    });
  }

  handleTransfer() {
    if (this.props.onTransfer) {
      // console.log("handleTransfer")
      this.props.onTransfer(this.props.dialect);
    }
  }

  handleEdit() {
    console.log("DialectItem handleEdit");
    this.props.onEdit(this.props.dialect);
  }

  handleDelete() {
    console.log("DialectItem handleDelete");
    this.props.onDelete(this.props.dialect);
  }


   renderInfo(styles) {
 
      return (
        <View style={styles.infoItem} >
          <Text style={styles.infoText}>{this.props.dialect.type}</Text>
        </View>
      );
  
  }

  renderCenterElement(styles) {
    return (
      <View style={styles.centerItemContainer}>
        <TouchableOpacity
          onPress={this.navigateToVocabulary}
          onLongPress={this.handleDelete}
        >
          <View style={styles.contentContainer}>
            
            <View style={styles.nameContainer}>
              <Text style={styles.nameText}>
                {this.props.dialect.name}
              </Text>
            </View>
            <Text style={styles.dialect}>
              {this.props.dialect.description}
            </Text>

            <View style={styles.info}>
              {this.renderInfo(styles)}
            </View>


          </View>

        </TouchableOpacity>
      </View>
    );
  }

  // renderMatchCount(styles) {
  //   if (this.props.dialect.matchCount > 0) {
  //     return (
  //       <Text style={styles.smallGreyRight}>
  //         {`~ ${this.props.dialect.matchCount} ~`}
  //       </Text>
  //     );
  //   }
  // }

  renderRightElement(styles) {
    return (
      <View style={styles.rightItemContainer}>
        <DotsMenu
          style={{ icon: styles.rightIcon }}
          items={this.state.menuItems}
        />
      </View>
    );
  }

  renderLeftElement(styles) {
    const imagename  = this.props.dialect.name.split(" ")[0].toLowerCase();
    const uri = `https://s3.amazonaws.com/kezzy-jargon/${imagename}.jpg?jshdg`
    return (
      <View style={styles.leftItemContainer}>
        <Avatar
          size={40}
          image={
            (

           <Image style={styles.circle} source={{ uri:uri}} />
        
            )
          }
        />
      </View>
    );
  }

  render() {
    const styles = getStyles(this.context.uiTheme);
    if (this.props.dialect._id === "SPACER") {
        return (<View style={{height:75}}></View>);
    }
    return (
      <View style={styles.listItemContainer}>
        {this.renderLeftElement(styles)}
        {this.renderCenterElement(styles)}
        {this.renderRightElement(styles)}
      </View>
    );
  }
}

DialectItem.contextTypes = { uiTheme: PropTypes.object.isRequired };

const getStyles = function(theme) {
  return StyleSheet.create({
    listItemContainer: {
      padding: 16,
      paddingVertical: 8,
      flexDirection: "row",
      borderBottomWidth: 1,
      borderColor: "rgba(0, 0, 0, 0.05)"
    },
    leftItemContainer: { width: 56, flexShrink: 0, paddingTop: 10 },
    centerItemContainer: { width: 160, flexGrow: 1 },
    rightItemContainer: { width: 42 },
    contentContainer: { flex: 0, flexDirection: "column", paddingRight: 8 },
    nameText: { ...theme.typography.subheading, color: "#000" },
    statsContainer: { alignSelf: "flex-start", width: 42 },
    dialect: {
      ...theme.typography.body1,
      marginBottom: 8,
      color: theme.palette.secondaryTextColor
    },
    smallGreyRight: {
      fontSize: 11,
      color: theme.palette.disabledTextColor,
      textAlign: "right"
    },
    rightIcon: { marginBottom: 0, marginTop: 10 },
    iconContainerStyle: { width: 42, height: 25, alignItems: "flex-end" },
    deleteIcon: { fontSize: 18 },
    chatIcon: { fontSize: 18 },
    chatAlert: { color: theme.palette.alertColor },
    footNote: {
      fontSize: 11,
      color: theme.palette.disabledTextColor,
      textAlign: "left"
    },
 
    circle: {
      width: 40,
      height: 40,
      borderRadius: 20
    },

     info: {
      flex: 0,
      flexDirection: "row-reverse",
      flexWrap: "wrap",
      alignItems: "flex-start"
    },
    infoItem: {
      // marginTop: 8,
      backgroundColor: "#cce",
      paddingHorizontal: 8,
      marginHorizontal: 4,
      borderRadius: 4,
      // borderWidth: 1,
      // borderColor: "#339"
    },
    infoText: { fontSize: 10, color: "white" }
  });
};

export default DialectItem;
