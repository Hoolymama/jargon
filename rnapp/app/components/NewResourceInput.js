import React, { Component, PropTypes } from "react";

import { ActionButton } from "react-native-material-ui";

// import ResourceEdit from "./ResourceEdit";
import { Modal } from "react-native";
class NewResourceInput extends Component {
  constructor(props) {
    super(props);
    this.onSave = this.onSave.bind(this);
    this.onClose = this.onClose.bind(this);
    this.handleActionButtonPress = this.handleActionButtonPress.bind(this);

    this.state = { showDialog: false, text: null };
  }

  static propTypes() {
    return {
      onSave: PropTypes.func.isRequired,
      onClose: PropTypes.func.isRequired
    };
  }

  onSave(event) {
    this.setState({ showDialog: false });
    if (this.props.onSave) {
      this.props.onSave(event);
    }
  }

  onClose() {
    this.setState({ showDialog: false });
    if (this.props.onClose) {
      this.props.onClose();
    }
  }

  handleActionButtonPress() {
    console.log("PRESSED ACTION BUTTON");
    this.setState({ showDialog: true });
  }

  render() {
    if (this.state.showDialog) {
      return (
        <Modal
          animationType={"slide"}
          transparent
          visible={this.state.visible}
          onRequestClose={this.onClose}
        >
        {this.props.children}
          <ResourceEdit
            initialText={this.state.initialText}
            onSave={this.onSave}
            onClose={this.onClose}
          />
        </Modal>
      );
    }
    return <ActionButton onPress={this.handleActionButtonPress} />;
  }
}

NewResourceInput.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default NewResourceInput;

// <NewResourceInputModal
//   onClose={this.handleModalCancel}
//   onSave={this.handleModalSave}
//   visible={this.state.showDialog}
// />
//
