import React, {Component, PropTypes} from "react";
import Meteor from "react-native-meteor";

import {Text, View, StyleSheet, Platform} from "react-native";
import {ListItem} from "react-native-material-ui";
import Icon from "react-native-vector-icons/FontAwesome";

import ImagePicker from "react-native-image-picker";

import {RNS3} from "react-native-aws3";
import AppConfig from "../AppConfig";

import ImageResizer from "react-native-image-resizer";

import AvatarCropperModal from "./AvatarCropperModal";
import Toast from "@remobile/react-native-toast";

const s3_options = {
  region: AppConfig.GetEnv("S3_REGION"),
  accessKey: AppConfig.GetEnv("S3_ACCESS_KEY"),
  secretKey: AppConfig.GetEnv("S3_SECRET_KEY"),
  keyPrefix: `${AppConfig.GetEnv("S3_PREFIX")}/avatars/`,
  bucket: "dytto",
  successActionStatus: 201
};

class AvatarChooser extends Component {
  constructor(props) {
    super(props);

    this.handleBrowser = this.handleBrowser.bind(this);
    this.handleCamera = this.handleCamera.bind(this);
    this.handleImage = this.handleImage.bind(this);
    this.sendToS3 = this.sendToS3.bind(this);
    this.handleServiceAvatar = this.handleServiceAvatar.bind(this);
    this.handleInitialsAvatar = this.handleInitialsAvatar.bind(this);

    this.handleCropperModalCancel = this.handleCropperModalCancel.bind(this);
    this.handleCropperModalSave = this.handleCropperModalSave.bind(this);

    this.state = { cropperModalVisible: false };
  }

  static propTypes() {
    return {
      currentUser: PropTypes.object.isRequired,
      size: PropTypes.number,
      color: PropTypes.string,
      opacity: PropTypes.number
    };
  }

  sendToS3(file) {
    RNS3.put(file, s3_options).then(response => {
      if (response.status === s3_options.successActionStatus) {
        const rn = Math.floor(Math.random() * 100000000);
        const decachedLocation = `${response.body.postResponse.location}?decache=${rn}`;
        Meteor.call("updateUser", { avatar: decachedLocation }, err => {
          if (err) {
            Toast.showShortTop("Couldn't set new image");
          }
        });
      } else {
        Toast.showShortTop("Upload failed or cancelled");
      }
    });
  }

  handleCropperModalCancel() {
    this.setState({ cropperModalVisible: false });
  }
  handleCropperModalSave(croppedImageUri) {
    console.log(croppedImageUri);

    this.sendToS3({
      uri: croppedImageUri,
      name: this.props.currentUser._id + ".jpg",
      type: "image/jpeg"
    });

    this.setState({ cropperModalVisible: false });
  }

  handleImage(response) {
    if (response.didCancel) {
      Toast.showShortTop("Cancelled image picker");
    } else if (response.error) {
      Toast.showShortTop("ImagePicker Error: " + response.error);
    } else {
      /* The main reason we resize is to apply the original rotation */
      ImageResizer
        .createResizedImage(response.uri, 1024, 1024, "JPEG", 100, 0)
        .then(resizedImageUri => {
          // resizeImageUri is the URI of the new image that can now be displayed, uploaded...
          // console.log(resizedImageUri)
          this.setState({
            cropperImageUri: resizedImageUri,
            cropperModalVisible: true
          });
        })
        .catch(err => {
          Toast.showShortTop("Error with image resizer");
        });
    }
  }

  handleBrowser() {
    ImagePicker.launchImageLibrary(
      { title: "Select Avatar", noData: true },
      this.handleImage
    );
  }

  handleCamera() {
    ImagePicker.launchCamera(
      { title: "Select Avatar", noData: true },
      this.handleImage
    );
  }

  handleServiceAvatar() {
    Meteor.call("setServiceAvatar", err => {
      if (err) {
        Toast.showShortTop("Upload failed or cancelled");
      }
    });
  }

  handleInitialsAvatar() {
    Meteor.call("setInitialsAvatar", err => {
      if (err) {
        Toast.showShortTop("Upload failed or cancelled");
      }
    });
  }

  renderServiceIcon() {
    const iconNames = { facebook: "facebook-square", google: "google" };

    const type = this.props.currentUser.serviceType;

    if (type !== "google" && type !== "facebook") {
      return null;
    }

    const cap = type[0].toUpperCase() + type.substring(1);

    return <ListItem key={4} dense leftElement={
      <Icon size={this.props.size} color={"#888"} name={iconNames[type]} />
    } centerElement={`${cap} profile image`} onPress={
      this.handleServiceAvatar
    } />;
  }

  renderCropper() {
    if (this.state.cropperModalVisible) {
      return <AvatarCropperModal onClose={
        this.handleCropperModalCancel
      } onSave={this.handleCropperModalSave} uri={
        this.state.cropperImageUri
      } />;
    }
    return null;
  }

  render() {
    const styles = getStyles(this.props);

    return (
      <View>
        <ListItem key={1} dense leftElement={
          <Icon size={this.props.size} color={"#888"} name="camera" />
        } centerElement="Take a Photo" onPress={this.handleCamera} />
        <ListItem key={2} dense leftElement={
          <Icon size={this.props.size} color={"#888"} name="image" />
        } centerElement="Browse the gallery" onPress={this.handleBrowser} />
        {this.renderServiceIcon()}
        <ListItem key={3} dense leftElement={<View>
            <Icon size={this.props.size} color={"#888"} name="circle" />
            <View style={
              { position: "absolute", top: 0, left: 4 }
            }><Text style={styles.circleText}>A</Text></View>
          </View>} centerElement="Initials" onPress={
          this.handleInitialsAvatar
        } />
        {this.renderCropper()}
      </View>
    );
  }
}

const getStyles = function(props) {
  return StyleSheet.create({
    iconContainer: {
      flex: 1,
      flexDirection: "row",
      justifyContent: "space-between"
    },
    iconRow: { flex: 1, flexDirection: "row", opacity: props.opacity },
    circleText: {
      backgroundColor: "transparent",
      color: "white",
      fontSize: props.size * 0.6,
      fontWeight: "bold",
      lineHeight: Platform.OS === "ios" ? 20 : 18,
      textAlign: "center"
    },
    buttonContainer: {},
    buttonText: { color: props.color }
  });
};

AvatarChooser.defaultProps = { size: 20, opacity: 1, alwaysOpen: false };

export default AvatarChooser