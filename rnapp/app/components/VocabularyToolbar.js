import React, { Component, PropTypes } from "react";
import Meteor from "react-native-meteor";

import { Text, StyleSheet, View } from "react-native";

import { Toolbar, IconToggle, Icon } from "react-native-material-ui";

import VToolbar from "./VToolbar";

import DotsMenu from "./DotsMenu";

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from "react-native-popup-menu";

class VocabularyToolbar extends Component {
  constructor(props) {
    super(props);
    this.onTriggerPress = this.onTriggerPress.bind(this);

    this.state = { menuOpen: false };
  }

  static propTypes() {
    return {
      onBackButton: PropTypes.func.isRequired,
      title:  PropTypes.string.isRequired,
      menuItems:  PropTypes.array.isRequired,
      currentUser: PropTypes.object,
      menuItems: PropTypes.array
    };
  }

  componentWillReceiveProps(nextProps) {}

  onTriggerPress() {
    this.setState({ menuOpen: true });
  }

  render() {
    const styles = getStyles(this.context.uiTheme);

    return (
      <View>
        <VToolbar
          leftElement="arrow-back"
          onLeftElementPress={this.props.onBackButton}
          centerElement={this.props.title}
          rightElement={
            (
              <DotsMenu
                style={{ icon: styles.toolbarIcon }}
                items={this.props.menuItems}
              />
            )
          }
           searchable={{
            autoFocus: true,
            placeholder: "Find"
          }}
        />
      </View>
    );
  }
}

VocabularyToolbar.contextTypes = { uiTheme: PropTypes.object.isRequired };

export default VocabularyToolbar;

const getStyles = function(theme) {
  return StyleSheet.create({
    toolbarIcon: { color: theme.palette.alternateTextColor }
  });
};
