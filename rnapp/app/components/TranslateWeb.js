import React, { Component, PropTypes } from "react";
import { Text, View, WebView, Networking,TextInput } from "react-native";
import JTextField from "./JTextField/TextField";
import { Button, ActionButton, Icon } from "react-native-material-ui";

// async function getPage() {
//   try {
//     let response = await fetch(
//       "https://magazine.artstation.com/2015/08/katia-bourykina-career-tips/"
//     );
//     let responseJson = await response.json();
//     return responseJson.movies;
//   } catch (error) {
//     console.error(error);
//   }
// }

class TranslateWeb extends Component {
  constructor(props) {
    super(props);

    this.handleUrlChange = this.handleUrlChange.bind(this);
    this.state = {
      content: "",
      url: "https://magazine.artstation.com/2015/08/katia-bourykina-career-tips/"
    };
  }

  static propTypes() {
    return {
      navigator: PropTypes.object.isRequired,
      transcriptions: PropTypes.array.isRequired
    };
  }
  handleUrlChange() {}

  componentDidMount() {
    fetch(this.state.url)
      .then(response => {
        return response.text();
      })
      .then(text => {
        // console.log("text");
        // console.log(text);

        this.setState({ content: text });

        return text;
      })
      .catch(error => {
        console.error(error);
      });
  }

  //  renderMovies() {
  //   return this.state.movies.map((m, i) => {

  //    console.log(m.title+" "+i)
  //    return <Text key={i}>{m.title}</Text>;
  //  });
  // }

  // "https://magazine.artstation.com/2015/08/katia-bourykina-career-tips/"
  render() {
    return (
      <View style={{ flex: 1, padding: 0, margin: 0 }}>
        <View
          style={{
            borderWidth: 1,
            borderColor: "red"
          }}
        >

          <TextInput
            value={this.state.url}
            onChange={this.handleUrlChange}
            returnKeyType="done"
            textColor="#555" 
          />
        </View>
        <WebView
          source={{ html: this.state.content }}
          onLoad={() => {
            console.log("Loading");
          }}
        />
        <ActionButton icon="link" onPress={this.handleChooseUrl} />
      </View>
    );
  }
}

// <View>
//         <Text>{this.state.content}</Text>
//       </View>
// <WebView
//   source={{ uri: "https://magazine.artstation.com/2015/08/katia-bourykina-career-tips/" }}
//   style={{ marginTop: 20 }}
// />

TranslateWeb.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default TranslateWeb;
