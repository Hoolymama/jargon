"use strict";
import Meteor from "react-native-meteor";
import React, { Component, PropTypes } from "react";
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  BackHandler,
  AsyncStorage
} from "react-native";
import { Icon, Drawer, ListItem, Button } from "react-native-material-ui";
import AvatarFallback from "./AvatarFallback";
// import AvatarChooser from "./AvatarChooser";
import SettingsInfoPanel from "./SettingsInfoPanel";
import EditNameDialog from "./EditNameDialog";
import ConfirmDialog from "./ConfirmDialog";
import VToolbar from "./VToolbar";

// import { GoogleSignin } from "react-native-google-signin";
// import { FBLoginManager } from "react-native-facebook-login";

import { routes } from "../routes";

// import AppConfig from "../AppConfig";
// import timer from "react-native-timer";

// const version = AppConfig.GetEnv("version");
// const build = AppConfig.GetEnv("build");
// const buildDate = AppConfig.GetEnv("buildDate");
// const env = AppConfig.GetEnv("buildEnvironment");

const getStyles = function(theme) {
  return StyleSheet.create({
    container: { flex: 1 },
    header: { backgroundColor: theme.palette.secondaryColor, flex: 1 },
    headerInner: { flex: 0, flexDirection: "row", margin: 15, marginBottom: 5 },
    groupTitleView: { marginLeft: 32, marginTop: 24 },
    groupTitleText: { fontWeight: "bold", color: theme.palette.primaryColor }
  });
};

class Settings extends Component {
  constructor(props) {
    super(props);
    this.handleNavigatorBack = this.handleNavigatorBack.bind(this);
    // this.handleLogout = this.handleLogout.bind(this);
    this.handleNameModalOpen = this.handleNameModalOpen.bind(this);
    this.handleNameModalClose = this.handleNameModalClose.bind(this);
    this.handleNameModalSubmit = this.handleNameModalSubmit.bind(this);

    this.handleCancelDeleteAccount = this.handleCancelDeleteAccount.bind(this);
    this.handleDoDeleteAccount = this.handleDoDeleteAccount.bind(this);
    this.handleShowDeleteAccount = this.handleShowDeleteAccount.bind(this);

    console.log("Settings props");
    console.log(props.offlineMode);
    console.log(props.currentUser);


    this.state = {
      nameModalVisible: false,
      deleteAccountDialogVisible: false,
      showActivityIndicator: false
    };
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", () => {
      if (
        this.props.navigator &&
        this.props.navigator.getCurrentRoutes().length > 1
      ) {
        this.handleNavigatorBack();
        return true;
      }
      return false;
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress");
  }

  static propTypes() {
    return {
      navigator: PropTypes.object.isRequired,
      currentUser: PropTypes.number.isRequired,
      onLogOut: PropTypes.func.isRequired
    };
  }

  handleNavigatorBack() {
    this.props.navigator.pop();
  }

  // handleLogout() {
  //   if (currentUser.serviceType === "offine") {
  //      AsyncStorage.removeItem("reactnativemeteor_usertoken", "offline");
  //   }
  //   Meteor.call("updateUser", { playerId: null });
  //   Meteor.logout();
  // }

  handleNameModalOpen() {
    this.setState({ nameModalVisible: true });
  }

  handleNameModalClose() {
    this.setState({ nameModalVisible: false });
  }

  handleNameModalSubmit(text) {
    Meteor.call("updateUser", { name: text });
    this.setState({ nameModalVisible: false });
  }

  handleCancelDeleteAccount() {
    this.setState({ deleteAccountDialogVisible: false });
  }

  handleDoDeleteAccount() {
    Meteor.call("removeUser");
    this.setState({
      deleteAccountDialogVisible: false,
      showActivityIndicator: true
    });
  }

  handleShowDeleteAccount() {
    this.setState({ deleteAccountDialogVisible: true });
  }

  renderActivity() {
    if (this.state.showActivityIndicator) {
      return (
        <ActivityIndicator
          color={this.context.uiTheme.palette.primaryColor}
          animating
          style={{
            height: 80,
            alignItems: "center",
            justifyContent: "center",
            padding: 8
          }}
          size="large"
        />
      );
    }
  }

  render() {
    const styles = getStyles(this.context.uiTheme);

    return (
      <View style={{ flex: 1 }}>
        <VToolbar
          leftElement="arrow-back"
          onLeftElementPress={this.handleNavigatorBack}
          centerElement="Settings"
          rightElement={
            (
              <Button
                style={{ text: { color: "white" } }}
                text="Log out"
                onPress={this.props.onLogout}
              />
            )
          }
          style={{
            centerElementContainer: {
              flexDirection: "row",
              justifyContent: "center"
            }
          }}
        />

        <View style={styles.container}>
          <Drawer>
            <Drawer.Header style={{ contentContainer: styles.header }}>
              <View style={styles.headerInner}>
                <AvatarFallback user={this.props.currentUser} radius={40} />
                <SettingsInfoPanel currentUser={this.props.currentUser} />
              </View>
            </Drawer.Header>
            {this.renderActivity()}
            <View style={styles.groupTitleView}>
              <Text style={styles.groupTitleText}>Personal</Text>
            </View>
            <ListItem
              dense
              leftElement={<Icon name="edit" />}
              centerElement="Edit Display Name"
              onPress={this.handleNameModalOpen}
            />

            <View style={styles.groupTitleView}>
              <Text style={styles.groupTitleText}>Account</Text>
            </View>
            <ListItem
              dense
              leftElement={<Icon name="delete" />}
              centerElement="Delete Account"
              onPress={this.handleShowDeleteAccount}
            />
          </Drawer>
        </View>

        <EditNameDialog
          onClose={this.handleNameModalClose}
          onSubmit={this.handleNameModalSubmit}
          visible={this.state.nameModalVisible}
          title="Set display name"
          initialText={this.props.currentUser.name}
        />

        <ConfirmDialog
          onNo={this.handleCancelDeleteAccount}
          onYes={this.handleDoDeleteAccount}
          visible={this.state.deleteAccountDialogVisible}
          title="Delete your account"
          message={`Are you sure?\nAll your dialects will also be deleted.`}
          yesButtonText="Delete"
          noButtonText="Cancel"
        />

      </View>
    );
  }
}

Settings.contextTypes = { uiTheme: PropTypes.object.isRequired };
export { Settings as LOC };

export default Settings;

// <View style={styles.groupTitleView}>
//            <Text style={styles.groupTitleText}>Fakes</Text>
//          </View>
//          <ListItem
//            dense
//            leftElement={<Icon name="person" />}
//            centerElement="Fakes"
//            onPress={this.navigateToFakes}
//          />
// <ListItem
//            dense
//            leftElement={<Icon name="date-range" />}
//            centerElement={`Build date: ${buildDate}`}
//          />
