import React, { Component, PropTypes } from "react";


import TranscriptionEdit from "./TranscriptionEdit";
import { Modal } from "react-native";
class TranscriptionInputModal extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes() {
    return {
      onSave: PropTypes.func.isRequired,
      onClose: PropTypes.func.isRequired,
      visible: PropTypes.bool,
      transcription: PropTypes.object.required
    };
  }
 
  render() {
    if (this.props.visible) {
      return (
        <Modal
          animationType={"slide"}
          transparent
          visible={this.props.visible}
          onRequestClose={this.props.onClose}
        >
          <TranscriptionEdit {...this.props} />
        </Modal>
      );
    }
    return null;
  }
}

TranscriptionInputModal.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default TranscriptionInputModal;
