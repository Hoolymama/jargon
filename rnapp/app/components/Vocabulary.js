import React, { Component, PropTypes } from "react";
import Meteor from "react-native-meteor";
// import { ActionButton } from "react-native-material-ui";
import { View, Platform, BackHandler, Text, StyleSheet } from "react-native";
import { routes } from "../routes";
import DotsMenu from "./DotsMenu";
import VocabularyToolbar from "./VocabularyToolbar";
import Toast from "@remobile/react-native-toast";
import VocabularyListView from "./VocabularyListView";
import TranscriptionInputModal from "./TranscriptionInputModal";
// import WithDatabase from "./WithDatabase";
import ConfirmDialog from "./ConfirmDialog";

import TranslateWeb from "./TranslateWeb";

import { TabViewAnimated, TabBar } from "react-native-tab-view";

class Vocabulary extends Component {
  constructor(props) {
    super(props);

    this.fetchTranscriptions = this.fetchTranscriptions.bind(this);
    this.handleNavigatorBack = this.handleNavigatorBack.bind(this);
    this.handleFabAddTranscription = this.handleFabAddTranscription.bind(this);
    this.handleCancelDelete = this.handleCancelDelete.bind(this);
    this.handleDoDelete = this.handleDoDelete.bind(this);
    this.handleShowDelete = this.handleShowDelete.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleShowEdit = this.handleShowEdit.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleFetchedResult = this.handleFetchedResult.bind(this);

    this.handleChangeTab = this.handleChangeTab.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderTabScene = this.renderTabScene.bind(this);

    this.state = {
      deleteDialogVisible: false,
      inputVisible: false,

      transcriptionToDelete: null,
      fetchingTranscriptions: false,
      transcriptions: [],
      initialTranscription: {},
      menuItems: [
        {
          label: "Something",
          disabled: false
          // callback: () => {
          //   this.handleShowDelete({
          //     source: { text: "ALL TRANSCRIPTIONS" },
          //     _id: "DELETEALLTRANSCRIPTIONS"
          //   });
          // }
        }
      ],

      tabs: {
        index: 0,
        routes: [
          { key: "1", title: "Vocabulary" },
          { key: "2", title: "Translate Web" },
          { key: "3", title: "Translate Text" }
        ]
      }
    };
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", () => {
      if (
        this.props.navigator &&
        this.props.navigator.getCurrentRoutes().length > 1
      ) {
        this.handleNavigatorBack();
        return true;
      }
      return false;
    });
    this.fetchTranscriptions();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress");
  }

  handleNavigatorBack() {
    this.props.navigator.pop();
  }

  static propTypes() {
    return {
      currentUser: PropTypes.object.isRequired,
      navigator: PropTypes.object.isRequired,
      dialect: PropTypes.object.isRequired,
      db: React.PropTypes.objectOf(React.PropTypes.func).isRequired
    };
  }

  handleFetchedResult(err, result) {
    if (err) {
      Toast.showShortTop("Fetch transcriptions - failed" + err);
    } else {
      if (Array.isArray(result)) {
        this.setState({ transcriptions: result });
      }
    }
    this.setState({ fetchingTranscriptions: false });
  }

  fetchTranscriptions() {
    this.setState({ fetchingTranscriptions: true });
    this.props.db.findTranscriptions(
      this.props.dialect,
      this.handleFetchedResult
    );
  }

  handleCancelDelete() {
    this.setState({
      deleteTranscriptionId: null,
      deleteDialogVisible: false
    });
  }

  handleDoDelete() {
    const t = this.state.transcriptionToDelete;
    t.type = this.props.dialect.type;
    this.props.db.removeTranscription(t, (err, num) => {
      if (!err) {
        Toast.showShortTop("Removed transcription: " + t.name);
        this.fetchTranscriptions();
      } else {
        Toast.showShortTop(err);
      }
    });

    this.setState({
      transcriptionToDelete: null,
      deleteDialogVisible: false
    });
  }

  handleShowDelete(transcription) {
    this.setState({
      transcriptionToDelete: {
        _id: transcription._id,
        name: transcription.source.text
      },
      deleteDialogVisible: true
    });
  }

  handleShowEdit(transcription) {
    this.setState({
      inputVisible: true,
      initialTranscription: transcription
    });
  }

  handleSave(transcription) {
    this.props.db.saveTranscription(
      this.props.dialect,
      transcription,
      this.fetchTranscriptions
    );
    this.setState({ inputVisible: false });
  }

  handleCloseModal() {
    this.setState({ inputVisible: false });
  }

  handleFabAddTranscription() {
    this.setState({
      inputVisible: true,
      initialTranscription: {
        source: { text: "" },
        outcomes: [{ text: "" }]
      }
    });
  }

  ////////////////// TABS
  handleChangeTab(index) {
    const tabs = JSON.parse(JSON.stringify(this.state.tabs));
    tabs.index = index;
    this.setState({ tabs });
  }
  renderHeader(props) {
    const styles = getStyles(this.context.uiTheme);
    return (
      <TabBar
        {...props}
        style={styles.tabbar}
        labelStyle={styles.tablabel}
        indicatorStyle={styles.indicator}
      />
    );
  }

  renderTabScene({ route: route }) {
    switch (route.key) {
      case "1":
        return (
          <VocabularyListView
            navigator={this.props.navigator}
            transcriptions={this.state.transcriptions}
            onDelete={this.handleShowDelete}
            onEdit={this.handleShowEdit}
            onShow={this.handleShowDetail}
            onRefresh={this.fetchVocabulary}
            refreshing={this.state.fetchingTranscriptions}
            onAddTranscription={this.handleFabAddTranscription}
          />
        );
      case "2":
        return (<TranslateWeb  transcriptions={this.state.transcriptions}/>);
      default:
        return <View style={{ flex: 1 }}><Text>TEXT</Text></View>;
    }
  }

  render() {
    const self = this;
    const transcriptionName = this.state.transcriptionToDelete &&
      this.state.transcriptionToDelete.name;
    const styles = getStyles(this.context.uiTheme);

    return (
      <View style={{ flex: 1 }}>

        <VocabularyToolbar
          leftElement="arrow-back"
          onBackButton={this.handleNavigatorBack}
          title={this.props.dialect.name}
          menuItems={this.state.menuItems}
        />

        <TabViewAnimated
          style={{ flex: 1 }}
          navigationState={this.state.tabs}
          renderScene={this.renderTabScene}
          renderHeader={this.renderHeader}
          onRequestChangeTab={this.handleChangeTab}
        />

  
        <TranscriptionInputModal
          visible={this.state.inputVisible}
          onSave={this.handleSave}
          onClose={this.handleCloseModal}
          transcription={this.state.initialTranscription}
        />

        <ConfirmDialog
          onNo={this.handleCancelDelete}
          onYes={this.handleDoDelete}
          visible={this.state.deleteDialogVisible}
          title="Delete Transcription"
          message={`\"${transcriptionName}\"\n\nAre you sure?`}
          yesButtonText="Delete"
          noButtonText="Cancel"
        />

      </View>
    );
  }
}
export default Vocabulary;

Vocabulary.contextTypes = { uiTheme: PropTypes.object.isRequired };

const getStyles = function(theme) {
  return StyleSheet.create({
    toolbarIcon: { color: theme.palette.alternateTextColor },
    tabbar: {
      backgroundColor: theme.palette.secondaryColor
    },
    tablabel: {
      color: "#fff",
      fontWeight: "600",
      fontSize: 10,
      textAlign: "center"
    },
    indicator: {
      backgroundColor: theme.palette.accentColor,
      height: 5
    }
  });
};
