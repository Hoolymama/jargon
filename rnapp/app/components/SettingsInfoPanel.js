"use strict";
import React, { Component, PropTypes } from "react";
import { Text, View, StyleSheet } from "react-native";
// import Geocoder from "../lib/Geocoder";
// import Config from "react-native-config"
// import AppConfig from "../AppConfig";
//console.log(AppConfig.GetEnv())
const styles = StyleSheet.create({
  container: { flex: 1 },
  nameRow: { flex: 0 },
  name: {
    color: "white",
    fontSize: 24,
    textAlign: "right",
    fontWeight: "bold"
  },
  addressTitle: {
    color: "white",
    textAlign: "right",
    fontSize: 13,
    fontWeight: "bold"
  },
  addressBody: { color: "white", textAlign: "right", fontSize: 13 },
  addressCoords: {
    color: "yellow",
    textAlign: "right",
    fontSize: 11,
    marginTop: 5
  }
});

class SettingsInfoPanel extends Component {
  constructor(props) {
    super(props);
 
 
  }

  static propTypes() {
    return { currentUser: PropTypes.number.isRequired };
  }

  componentWillReceiveProps(props) {
    // this.updateAddress(props.currentUser.location);
  }



  renderName() {
    return (
      <View style={styles.nameRow}>
        <Text style={styles.name}>{this.props.currentUser.name}</Text>
      </View>
    );
  }


  render() {
    return (
      <View style={styles.container}>
        {this.renderName()}
        
      </View>
    );
  }
}

export { SettingsInfoPanel as LOC };

export default SettingsInfoPanel;
