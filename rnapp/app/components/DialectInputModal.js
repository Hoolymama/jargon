import React, { Component, PropTypes } from "react";


import DialectEdit from "./DialectEdit";
import { Modal } from "react-native";

class DialectInputModal extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes() {
    return {
      onSave: PropTypes.func.isRequired,
      onClose: PropTypes.func.isRequired,
      dialect: PropTypes.object.required,
      visible: PropTypes.bool,
    };
  }
 
  render() {
    if (this.props.visible) {
      return (
        <Modal
          animationType={"slide"}
          transparent
          visible={this.props.visible}
          onRequestClose={this.props.onClose}
        >
          <DialectEdit {...this.props} />
        </Modal>
      );
    }
    return null;
  }
}

DialectInputModal.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default DialectInputModal;
