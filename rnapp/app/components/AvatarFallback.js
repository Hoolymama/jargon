import React, {Component, PropTypes} from "react";

import {View, StyleSheet, Image} from "react-native";
import {Avatar} from "react-native-material-ui";

const getStyles = function(props) {
  return StyleSheet.create({
    box: { width: props.radius * 2, height: props.radius * 2 },
    circle: {
      width: props.radius * 2,
      height: props.radius * 2,
      borderRadius: props.radius
    },
    container: { backgroundColor: props.user.color },
    initialsContent: { fontSize: props.radius * 0.7, fontWeight: "bold" }
  });
};

class AvatarFallback extends Component {
  static propTypes() {
    return {
      user: PropTypes.object.isRequired,
      radius: PropTypes.number,
      mine: PropTypes.bool,
      online: PropTypes.bool
    };
  }

  renderImage() {
    const styles = getStyles(this.props);

    return <Avatar size={this.props.radius * 2} image={
      <Image style={styles.circle} source={{ uri: this.props.user.avatar }} />
    } />;
  }

  renderInitials() {
    const styles = getStyles(this.props);
    let initials = "??";
    if (this.props.user && this.props.user.name) {
      initials = this.props.user.name
        .split(" ")
        .slice(0, 3)
        .filter(el => el.length > 0)
        .map(word => word[0].toUpperCase())
        .join("");
    }
    return <Avatar size={this.props.radius * 2} text={initials} style={
      { content: styles.initialsContent, container: styles.container }
    } />;
  }

  renderAvatar() {
    if (this.props.user.avatar && this.props.user.avatar !== "initials") {
      return this.renderImage();
    } else {
      return this.renderInitials();
    }
  }

  render() {
    const styles = getStyles(this.props);
    return (
      <View style={styles.box}>
        {this.renderAvatar()}
      </View>
    );
  }
}
AvatarFallback.defaultProps = { radius: 20, mine: false, online: false };
export default AvatarFallback