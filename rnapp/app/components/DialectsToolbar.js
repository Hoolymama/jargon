import React, { Component, PropTypes } from "react";
import Meteor from "react-native-meteor";

import { Text, StyleSheet, View } from "react-native";

import { Toolbar, IconToggle, Icon } from "react-native-material-ui";

import VToolbar from "./VToolbar";

import DotsMenu from "./DotsMenu";

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from "react-native-popup-menu";

class DialectsToolbar extends Component {
  constructor(props) {
    super(props);
    this.onTriggerPress = this.onTriggerPress.bind(this);

    this.state = { menuOpen: false };
  }

  static propTypes() {
    return {
      onSettingsButton: PropTypes.func.isRequired,
      currentUser: PropTypes.object,
      menuItems: PropTypes.array
    };
  }

  componentWillReceiveProps(nextProps) {}

  onTriggerPress() {
    this.setState({ menuOpen: true });
  }

  render() {
    const styles = getStyles(this.context.uiTheme);

    return (
      <View>
        <VToolbar
          leftElement="settings"
          onLeftElementPress={this.props.onSettingsButton}
          centerElement="Dialects"
          rightElement={
            (
              <DotsMenu
                style={{ icon: styles.toolbarIcon }}
                items={this.props.menuItems}
              />
            )
          }
        />
      </View>
    );
  }
}

DialectsToolbar.contextTypes = { uiTheme: PropTypes.object.isRequired };

export default DialectsToolbar;

const getStyles = function(theme) {
  return StyleSheet.create({
    toolbarCenter: {
      flex: 1,
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center"
    },
    toolbarIcon: { color: theme.palette.alternateTextColor }
  });
};
