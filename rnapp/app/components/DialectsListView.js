import React, { Component, PropTypes } from "react";

import { ListView, RefreshControl, ActivityIndicator } from "react-native";

import DialectItem from "./DialectItem";

import EmptyDialectsPage from "./EmptyDialectsPage" 

class DialectsListView extends Component {
  constructor(props) {
    super(props);

    this.renderOneDialect = this.renderOneDialect.bind(this);
    this.rowChanged = this.rowChanged.bind(this);

    this.listViewDataSource = new ListView.DataSource({
      rowHasChanged: this.rowChanged
    });

    const dialects = props.dialects || [];
    dialects.push({_id: "SPACER"})

    this.state = {
      dataSource: this.listViewDataSource.cloneWithRows(dialects)
    };
  }

  static propTypes() {
    return {
      navigator: PropTypes.object.isRequired,
      onDelete: PropTypes.func.isRequired,
      onEdit: PropTypes.func.isRequired,
      onTransfer: PropTypes.func ,
      dialects: PropTypes.array.isRequired,
      refreshing: PropTypes.bool.isRequired,
      onRefresh: PropTypes.func.isRequired
    };
  }

  rowChanged(r1, r2) {
    return true;
  }

  componentWillReceiveProps(nextProps) {
    const dialects = nextProps.dialects || [];
    dialects.push({_id: "SPACER"})
    this.setState({
      dataSource: this.listViewDataSource.cloneWithRows(dialects)
    });
  }

  renderOneDialect(rowData) {
    return (
      <DialectItem
        {...this.props}
        dialect={rowData}
      />
      );
  }

  render() {
    if (!(this.props.dialects && this.props.dialects.length > 0)) {
      if (this.props.refreshing) {
        return (
          <ActivityIndicator
          color={this.context.uiTheme.palette.primaryColor}
          animating
          style={{
            height: 80,
            alignItems: "center",
            justifyContent: "center",
            padding: 8
          }}
          size="large"
          />
          );
      } else {
        return <EmptyDialectsPage />;
      }
    }

    return (
      <ListView
      initialListSize={6}
      enableEmptySections
      dataSource={this.state.dataSource}
      renderRow={this.renderOneDialect}
      refreshControl={
        (
          <RefreshControl
          colors={[
            this.context.uiTheme.palette.primaryColor,
            this.context.uiTheme.palette.accentColor
            ]}
            refreshing={this.props.refreshing}
            onRefresh={this.props.onRefresh}
            />
            )
      }
      />
      );
  }
}

DialectsListView.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default DialectsListView;
