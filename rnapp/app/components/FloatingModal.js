import React, {Component, PropTypes} from "react";
import {Modal, StyleSheet, View, Text, Dimensions} from "react-native";
import {Button} from "react-native-material-ui";

// const { width, height } = Dimensions.get("window")
// const DIALOG_MARGIN = Math.min(width, height)
class FloatingModal extends Component {
  constructor(props) {
    super(props);

    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.doNothing = this.doNothing.bind(this);
  }

  static propTypes() {
    return {
      animationType: PropTypes.string,
      onClose: PropTypes.func,
      onSubmit: PropTypes.func,
      visible: PropTypes.bool,
      children: React.PropTypes.node,
      title: PropTypes.string.isRequired,
      showDividers: PropTypes.bool,
      valid: PropTypes.bool,
      submitButtonText: PropTypes.string.isRequired,
      cancelButtonText: PropTypes.string.isRequired
    };
  }

  handleClose() {
    if (this.props.onClose) {
      this.props.onClose();
    }
  }

  handleSubmit() {
    if (this.props.onSubmit) {
      this.props.onSubmit();
    }
  }

  renderSubmitButton(styles) {
    if (this.props.valid) {
      return <Button style={{ fontWeight: "bold" }} primary text={
        this.props.submitButtonText
      } onPress={this.handleSubmit} />;
    } else {
      return <Button style={{ fontWeight: "bold" }} disabled text={
        this.props.submitButtonText
      } />;
    }
  }
  renderCancelButton(styles) {
    if (this.props.onClose) {
      return <Button style={{ fontWeight: "bold" }} primary text={
        this.props.cancelButtonText
      } onPress={this.handleClose} />;
    }
  }

  doNothing() {}

  render() {
    const styles = getStyles(this.props, this.context.uiTheme);

    // console.log(styles)
    return (
      <Modal animationType={this.props.animationType} transparent={
        true
      } visible={this.props.visible} onRequestClose={this.handleClose}>
        <View style={styles.lightbox}>
          <View style={styles.modalContainer}>
            <View style={styles.modalHeader}>
              <Text style={styles.titleText}>{this.props.title}</Text>
            </View>
            <View style={styles.modalContent}>
              {this.props.children}
            </View>
            <View style={styles.modalFooter}>
              {this.renderSubmitButton(styles)}
              {this.renderCancelButton(styles)}
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const getStyles = function(props, theme) {
  const dividerWidth = props.showDividers ? 1 : 0;

  const result = {
    lightbox: {
      flex: 1,
      backgroundColor: "rgba(0,0,0, 0.5)",
      justifyContent: "flex-start",
      alignItems: "center"
    },
    modalContainer: {
      backgroundColor: "white",
      marginTop: 100,
      opacity: 1,
      elevation: 5,
      width: 300,
      flex: 0
    },
    modalHeader: {
      padding: 24,
      paddingBottom: 20,
      borderBottomWidth: dividerWidth,
      borderColor: theme.palette.disabledTextColor
    },
    titleText: { color: "black", fontSize: 20 },
    modalContent: { flex: 0, marginLeft: 24, marginRight: 24 },
    modalFooter: {
      flex: 0,
      flexDirection: "row-reverse",
      padding: 8,
      borderTopWidth: dividerWidth,
      borderColor: theme.palette.disabledTextColor
    },
    modalButtons: { fontWeight: "bold" }
  };

  return result;
};

FloatingModal.contextTypes = { uiTheme: PropTypes.object.isRequired };
FloatingModal.defaultProps = {
  animationType: "slide",
  visible: false,
  showDividers: false,
  submitButtonText: "save",
  cancelButtonText: "cancel"
};

export default FloatingModal