import React, { Component, PropTypes } from "react";
import { StyleSheet, Text } from "react-native";
import { IconToggle } from "react-native-material-ui";

import Menu, {
  MenuOptions,
  MenuOption,
  MenuTrigger,
  NotAnimatedContextMenu
} from "react-native-popup-menu";

const getStyles = function(theme) {
  return StyleSheet.create({
    toolbarIcon: { color: theme.palette.alternateTextColor }
  });
};

class DotsMenu extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.onTriggerPress = this.onTriggerPress.bind(this);
    this.onBackdropPress = this.onBackdropPress.bind(this);
    this.state = { menuOpen: false };
  }

  onSelect(value) {
    this.setState({ menuOpen: false });
    this.props.items[value].callback();
  }

  onTriggerPress() {
    this.setState({ menuOpen: true });
  }

  onBackdropPress() {
    this.setState({ menuOpen: false });
  }

  static propTypes() {
    return {
      items: PropTypes.array.isRequired,
      style: PropTypes.shape,
    };
  }

  renderMenuOptions() {
    return this.props.items.map((item, i) => (
      <MenuOption
        key={i}
        value={i}
        disabled={item.disabled}
        text={item.label}
      />
    ));
  }

  render() {
    const styles = getStyles(this.context.uiTheme);

    return (
      <Menu
        key={2}
        opened={this.state.menuOpen}
        onBackdropPress={() => this.onBackdropPress()}
        onSelect={value => this.onSelect(value)}
      >
        <MenuTrigger>
          <IconToggle
            style={{ icon: this.props.style.icon }}
            name="more-vert"
            onPress={this.onTriggerPress}
          />
        </MenuTrigger>
        <MenuOptions
          customStyles={{
            optionsWrapper: { padding: 10, marginTop: 5 },
            optionWrapper: { paddingBottom: 10 }
          }}
        >
          {this.renderMenuOptions()}
        </MenuOptions>
      </Menu>
    );
  }
}
DotsMenu.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default DotsMenu;
