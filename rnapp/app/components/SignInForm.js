"use strict";
import React, { Component, PropTypes } from "react";
import Meteor, { Accounts } from "react-native-meteor";
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  ActivityIndicator
} from "react-native";
import VTextField from "./VTextField";
import { Button } from "react-native-material-ui";

import Toast from "@remobile/react-native-toast";
const { width, height } = Dimensions.get("window");
const PANEL_WIDTH = Math.min(Math.min(width, height), 500) - 60;

const getStyles = function(theme) {
  return StyleSheet.create({
    formContainer: {
      width: PANEL_WIDTH,
      padding: 16,
      flex: 0,
      marginTop: 40,
      alignItems: "center",
      borderRadius: 8,
      borderWidth: 1,
      borderColor: theme.palette.primaryColor,
      backgroundColor: "rgba(0,0,0,0.8)"
    },
    raisedButtonContainer: { borderRadius: 2, height: 40 },
    flatButtonContainer: { marginTop: 10 },
    flatButtonText: { color: "white", backgroundColor: "transparent" },
    working: {
      marginBottom: 10,
      textAlign: "center",
      fontSize: 30,
      fontWeight: "bold",
      color: "white"
    }
  });
};

class SignInForm extends Component {
  constructor(props) {
    super(props);

    this.handleUpdateName = this.handleUpdateName.bind(this);
    this.handleUpdateEmail = this.handleUpdateEmail.bind(this);
    this.handleUpdatePassword = this.handleUpdatePassword.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleError = this.handleError.bind(this);
    this.handleCreateAccount = this.handleCreateAccount.bind(this);
    this.handleChangeMode = this.handleChangeMode.bind(this);
    this.handleSetLoggingIn = this.handleSetLoggingIn.bind(this);


    this.state = {
      email: "julian.mann@gmail.com",
      password: "abc123",
      name: "Julian Mann",
      error: null,
      mode: "login",
      loggingIn: false
    };
  }
  
  static propTypes() {
    return {
      onPressOffline: PropTypes.func,
      isOnline: PropTypes.bool
    };
  }

  handleError(error) {
    Toast.showShortTop(`Error: ${error.reason}`);
    this.setState({ loggingIn: false });
  }

  handleLogin() {
    const { email, password } = this.state;
    this.setState({ loggingIn: true });
    Meteor.loginWithPassword(email, password, error => {
      if (error) {
        this.handleError(error);
        this.setState({ loggingIn: false });
      }
    });
  }

  handleCreateAccount() {
    const { email, password, name } = this.state;

    this.setState({ loggingIn: true });
    Accounts.createUser(
      { email: email, password: password, name: name },
      error => {
        if (error) {
          this.handleError(error);
        } else {
          this.handleLogin();
        }
      }
    );
  }

  handleChangeMode() {
    this.setState({ mode: this.state.mode === "signup" ? "login" : "signup" });
  }

  handleUpdateName(text) {
    this.setState({ name: text });
  }

  handleUpdateEmail(text) {
    this.setState({ email: text });
  }

  handleUpdatePassword(text) {
    this.setState({ password: text });
  }

  handleSetLoggingIn(loggingIn) {
    console.log("SET LOGGING IN: " + loggingIn);
    this.setState({ loggingIn: loggingIn });
  }

  renderNameField(styles) {
    if (this.state.mode === "signup") {
      return (
        <VTextField
          placeholder="Name"
          onTextChange={this.handleUpdateName}
          value={this.state.name}
        />
      );
    }
  }

  renderTextFields(styles) {
    return (
      <View style={{ flex: 0, alignSelf: "stretch" }}>
        {this.renderNameField(styles)}
        <VTextField
          placeholder="Email"
          value={this.state.email}
          type="email"
          onTextChange={this.handleUpdateEmail}
        />
        <VTextField
          placeholder="Password"
          value={this.state.password}
          type="password"
          onTextChange={this.handleUpdatePassword}
        />
      </View>
    );
  }

  renderLoginButtons(styles) {
    if (this.state.mode === "login") {
      return (
        <View style={{ flex: 0 }}>
          <Button
            raised
            primary
            text="Log In"
            onPress={this.handleLogin}
            style={{ container: styles.raisedButtonContainer }}
          />
          <Button
            primary
            text="Create Account ->"
            onPress={this.handleChangeMode}
            style={{
              container: styles.flatButtonContainer,
              text: styles.flatButtonText
            }}
          />
        </View>
      );
    }
  }
  // If you forgot your password - too bad, for now at least
  // <Button
  // primary text="Forgot Password"
  // style={{container: styles.flatButtonContainer, text: styles.flatButtonText}} />
  renderSignupButtons(styles) {
    if (this.state.mode === "signup") {
      return (
        <View style={{ flex: 0 }}>
          <Button
            raised
            primary
            text="Create Account"
            onPress={this.handleCreateAccount}
            style={{ container: styles.raisedButtonContainer }}
          />
          <Button
            primary
            text="<- Log In"
            onPress={this.handleChangeMode}
            style={{
              container: styles.flatButtonContainer,
              text: styles.flatButtonText
            }}
          />
        </View>
      );
    }
  }
  renderOfflineButton(styles) {
    return (
      <View style={{ flex: 0 }}>
        <Button
          raised
          primary
          text="Offline Mode"
          onPress={this.props.onPressOffline}
          style={{ container: styles.raisedButtonContainer }}
        />
      </View>
    );
  }

  render() {
    const styles = getStyles(this.context.uiTheme);
    if (!this.state.loggingIn) {
      return (
        <View style={styles.formContainer}>
          {this.renderTextFields(styles)}
          {this.renderLoginButtons(styles)}
          {this.renderSignupButtons(styles)}
          {this.renderOfflineButton(styles)}

        </View>
      );
    }
    return (
      <ActivityIndicator
        color={this.context.uiTheme.palette.primaryColor}
        animating={true}
        style={{
          height: 80,
          alignItems: "center",
          justifyContent: "center",
          padding: 8
        }}
        size="large"
      />
    );
  }
}

SignInForm.contextTypes = { uiTheme: PropTypes.object.isRequired };

export default SignInForm;
