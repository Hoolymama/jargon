"use strict";
import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";

const getStyles = function() {
  return StyleSheet.create({
    page: { flex: 1, padding: 20, justifyContent: "center" },
    title: {
      marginBottom: 10,
      textAlign: "center",
      fontSize: 28,
      fontWeight: "bold",
      color: "white",
      backgroundColor: 'transparent'
    },
    heading: {
      marginTop: 10,
      marginBottom: 20,
      textAlign: "center",
      fontSize: 20,
      fontWeight: "bold",
      color: "white",
      backgroundColor: 'transparent'
    }
  });
};

class NotConnected extends Component {
  constructor(props) {
    super(props);
    console.log(props.status);
  }

  componentWillReceiveProps(props) {
    console.log(props.status);
  }

  static propTypes() {
    return { status: React.PropTypes.object.isRequired };
  }

  render() {
    const styles = getStyles();

    if (
      this.props.status.status === "offline" ||
        this.props.status.status === "disconnected"
    ) {
      return (
        <View style={styles.page}>
          <Text style={styles.title}>Trying to connect...</Text>
          <Text style={
            styles.heading
          }>A sign-in form will appear when the connection is established.</Text>
        </View>
      );
    }

    if (this.props.status.status === "connecting") {
      return (
        <View style={styles.page}>
          <Text style={styles.title}>Trying to connect...</Text>
          <Text style={
            styles.heading
          }>A sign-in form will appear when the connection is established.</Text>
        </View>
      );
    }

    if (this.props.status.status === "failed") {
      return (
        <View style={styles.page}>
          <Text style={styles.title}>Connetion failed</Text>
          <Text style={styles.heading}>{this.props.status.reason}</Text>
          <Text style={
            styles.heading
          }>A sign-in form will appear when the connection is established.</Text>
        </View>
      );
    }

    if (this.props.status.status === "waiting") {
      return (
        <View style={styles.page}>
          <Text style={
            styles.title
          }>Connetion failed and waiting to reconnect.</Text>
          <Text style={
            styles.heading
          }>A sign-in form will appear when the connection is established.</Text>
        </View>
      );
    }
    return null;
  }
}
export default NotConnected;
