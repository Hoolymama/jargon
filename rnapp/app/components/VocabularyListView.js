import React, { Component, PropTypes } from "react";
import { ListView, RefreshControl, ActivityIndicator , View} from "react-native";
import TranscriptionItem from "./TranscriptionItem";
import EmptyTranscriptionsPage from "./EmptyTranscriptionsPage";
import { ActionButton } from "react-native-material-ui";

class VocabularyListView extends Component {
  constructor(props) {
    super(props);

    this.renderOneTranscripton = this.renderOneTranscripton.bind(this);
    this.rowChanged = this.rowChanged.bind(this);

    this.listViewDataSource = new ListView.DataSource({
      rowHasChanged: this.rowChanged
    });

    const transcriptions = props.transcriptions || [];

    this.state = {
      dataSource: this.listViewDataSource.cloneWithRows(transcriptions)
    };
  }

  static propTypes() {
    return {
      navigator: PropTypes.object.isRequired,
      onDelete: PropTypes.func.isRequired,
      onEdit: PropTypes.func.isRequired,
      onShow: PropTypes.func.isRequired,
      transcriptions: PropTypes.array.isRequired,
      refreshing: PropTypes.bool.isRequired,
      onRefresh: PropTypes.func.isRequired,
      onAddTranscription: PropTypes.func.isRequired
    };
  }

  rowChanged(r1, r2) {
    return true;
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      dataSource: this.listViewDataSource.cloneWithRows(
        nextProps.transcriptions || []
      )
    });
  }

  renderOneTranscripton(rowData) {
    return (
      <TranscriptionItem
        navigator={this.props.navigator}
        transcription={rowData}
        onDelete={this.props.onDelete}
        onEdit={this.props.onEdit}
        onShow={this.props.onShow}
      />
    );
  }

  render() {
    if (!(this.props.transcriptions && this.props.transcriptions.length > 0)) {
      if (this.props.refreshing) {
        return (
          <ActivityIndicator
            color={this.context.uiTheme.palette.primaryColor}
            animating
            style={{
              height: 80,
              alignItems: "center",
              justifyContent: "center",
              padding: 8
            }}
            size="large"
          />
        );
      } else {
        return (
          <View style={{flex:1}}>
            <EmptyTranscriptionsPage />
            <ActionButton onPress={this.props.onAddTranscription} />
          </View>
        );
      }
    }

    return (
        <View style={{flex:1}}>
      <ListView

        initialListSize={6}
        enableEmptySections
        dataSource={this.state.dataSource}
        renderRow={this.renderOneTranscripton}
        refreshControl={
          (
            <RefreshControl
              colors={[
                this.context.uiTheme.palette.primaryColor,
                this.context.uiTheme.palette.accentColor
              ]}
              refreshing={this.props.refreshing}
              onRefresh={this.props.onRefresh}
            />
          )
        }
      />
                  <ActionButton onPress={this.props.onAddTranscription} />

   </View>

    );
  }
}

VocabularyListView.contextTypes = { uiTheme: PropTypes.object.isRequired };
export default VocabularyListView;
