
import tinycolor  from "tinycolor2"

const hexColorFromName = (name) => {

	var val = 0
	if (name.length == 0) return "#000"

	for (var i = 0; i < name.length; i++) {
		val = name.charCodeAt(i) + (val*i)  
	}
 
	val = val % 360
	const color =  new tinycolor(`hsv ${val} 0.4 0.6`)
	return color.toHexString()
}

export {hexColorFromName}