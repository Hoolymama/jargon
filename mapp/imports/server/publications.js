// import { Chats  } from "/imports/both/collection"

const currentUserFields = {fields: {
	"avatar":1,
	"emails": 1,
	"name": 1, 
	"serviceType": 1,
	"color": 1
}}

Meteor.publish("currentUserData", function () {
	return Meteor.users.find({_id: this.userId}, currentUserFields)
})

// Meteor.publish("chat", function (otherUserId) {
// 	return Chats.find( {	$and: [ 
// 		{ "participants._id": otherUserId}, 
// 		{ "participants._id": this.userId} 
// 	]})
// })

// const chatsListFields =   {
// 	"lastMessage": 1, 
// 	"participants": 1, 
// 	"_id": 1
// }

// Meteor.publish("chats", function () {
// 	return  Chats.find( { "participants._id": this.userId},  {fields:chatsListFields, sort: {"lastMessage.createdAt": -1}})
// })
