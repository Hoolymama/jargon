import  {hexColorFromName} from "../lib/ColorUtils"

Accounts.onCreateUser(function(options, user) {

	if (! options) options = {}

	user.avatar = null
	user.name = options.name
	user.serviceType = "password"
	user.color = hexColorFromName(user._id)
	return user
})



