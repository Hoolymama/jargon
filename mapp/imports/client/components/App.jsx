import React, { Component, PropTypes } from "react"

import { createContainer } from "meteor/react-meteor-data"

import {   Link } from "react-router"


const styles = {
	link: {
		marginLeft:20
	},
	container: {
		marginBottom:20
	}
}


class App extends Component {

	static propTypes() {
		return {
			currentUser: PropTypes.object
		}
	}

	renderMenu(){
		if (this.props.currentUser) {
			return (<div  style={styles.container}>
				<span style={styles.link}><Link to="/auth">Auth</Link></span>
				<span style={styles.link}> <Link to="/populate">Populate</Link></span>
				</div>
				)
		}
	}

	render() {

		return (<div>
			{this.renderMenu()}
			{this.props.children}
			</div>
			)
	}
}


export default createContainer(() => {
  Meteor.subscribe("currentUserData")
	return {
		loggingIn: Meteor.loggingIn(),
		currentUser: Meteor.user()
	}
}, App)


//				<span style={styles.link}> <Link to="/publications">Publications</Link></span>
