"use strict"

import React, { Component } from "react"
import { createContainer } from "meteor/react-meteor-data"


const styles = {
	entry: {
		marginBottom: 10
	},
	button: {
		width:200
	},
	label: {
		fontSize:12
	}
}
class Auth extends  Component {

	constructor(props) {
		super(props)
		this.handleUpdateName = this.handleUpdateName.bind(this)
		this.handleUpdateEmail = this.handleUpdateEmail.bind(this)
		this.handleUpdatePassword = this.handleUpdatePassword.bind(this)

		this.handleLogin = this.handleLogin.bind(this)
		this.handleError = this.handleError.bind(this)
		this.handleCreateAccount = this.handleCreateAccount.bind(this)
		this.handleChangeMode = this.handleChangeMode.bind(this)
		this.handleLogout = this.handleLogout.bind(this)

		

		this.state = {
			email:"",
			password: "",
			name: "",
			error: null,
			mode: "login"
		}
	}

	static propTypes() {
		return {
			currentUser: PropTypes.object 
		}
	}

	handleError(error){
		if (error) {
			this.setState({error: error.reason})
		}
	}

	handleLogin(){
		const { email, password } = this.state
		Meteor.loginWithPassword(email, password, (error) => {
			this.handleError(error)
		})
	}

	handleCreateAccount(){
		const { email, password, name } = this.state

		Accounts.createUser({ 
			email: email,
			password: password ,
			name: name
		}, 

		(error) => {
			if (error) {
				this.handleError(error)
			} else {
				// this.handleLogin()
			}
		})
	}

	handleChangeMode(){
		this.setState({
			mode: (this.state.mode === "signup") ? "login" : "signup"
		})
	}

	handleUpdateName(evt){
		this.setState({name: evt.target.value})
	}

	handleUpdateEmail(evt){
		this.setState({email: evt.target.value})
	}

	handleUpdatePassword(evt){
		this.setState({password: evt.target.value})
	}
	handleLogout(){
		Meteor.logout()
	}


	renderNameField(){
		if (this.state.mode === "signup") {
			return (
			<div  style={styles.entry}>
			<label style={styles.label}>Name</label><br />
			<input key={1} type="text"  value={this.state.name}  onChange={this.handleUpdateName} /> 
			</div>
				)
		}
	}

	renderEmailPassFields(){
		return (
			<div  style={styles.entry}>
			<label style={styles.label}>Email</label><br />
			<input key={2} type="text"  value={this.state.email}  onChange={this.handleUpdateEmail} /><br /> 
			<label style={styles.label}>Password</label><br />
			<input key={3} type="text"  value={this.state.password}  onChange={this.handleUpdatePassword} /><br /> 
			</div>
			)

	}

	renderLoginButtons(){
		if (this.state.mode === "login") {
			return(	
				<div style={styles.entry}>
				<button type="button" style={styles.button} onClick={this.handleLogin}>Login</button>
				</div>
				)
		}
	}

	renderSignupButtons(){
		if (this.state.mode === "signup") {
			return(	
				<div style={styles.entry}>
				<button type="button" style={styles.button} onClick={this.handleCreateAccount}>Create Account</button>
				</div>
				)
		}
	}

	renderSwitchButton(){
		return(	
				<div style={styles.entry}>
				<button  type="button"  style={styles.button} onClick={this.handleChangeMode}>Change to {this.state.mode === "signup" ? "Login" : "Signup"}</button>
				</div>
				)
	}

	render() {

		if (this.props.currentUser) {
			return (
				<div style={styles.entry}>
				<button type="button" style={styles.button} onClick={this.handleLogout}>Log Out {this.props.currentUser.name}</button>
				</div>
			 	)
		}

		return (

			<div>
			<fieldset>
		 
			{this.renderNameField()}
			{this.renderEmailPassFields()}
			{this.renderLoginButtons()}
			{this.renderSignupButtons()}
			{this.renderSwitchButton()}
			</fieldset>
			</div>
			)

	}
}

export default createContainer(( ) =>  {return {currentUser:  Meteor.user() } } , Auth)

