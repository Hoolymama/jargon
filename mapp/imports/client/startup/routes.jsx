import React from "react"

import { Router, Route, IndexRoute, hashHistory} from "react-router"
import { render } from "react-dom"




import App from "/imports/client/components/App"
import Auth from "/imports/client/components/Auth"

// import AccountsUIWrapper from "/imports/client/components/AccountsUIWrapper.jsx"
 
 

import injectTapEventPlugin from "react-tap-event-plugin"


export default class Routes extends React.Component {


	render() {
		return (  <Router history={hashHistory} >
    <Route path="/" component={App}>
      <IndexRoute component={Auth} />
      <Route path="/auth" component={Auth} />
    </Route>
  </Router>
  )

	}

}
     //<Route path="/publications" component={PublicationTest} />

      // <Route path="/populate" component={PopulateDatabase} />

      // <Route path="/people" component={PeopleIndex}/>
      // <Route path="/lists/:listId" component={TaskIndex}/>
      // <Route path="/invites/:token" component={AcceptInvite}/>
      // <Route path="/about" component={About}/>