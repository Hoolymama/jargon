import React from "react"
import { Meteor } from "meteor/meteor"
import { render } from "react-dom"
// import  App from '/imports/client/components/App'

import  Routes from "/imports/client/startup/routes"

import injectTapEventPlugin from "react-tap-event-plugin"

injectTapEventPlugin()

Meteor.startup(function () {
	render(<Routes />, document.getElementById("render-target"))
})