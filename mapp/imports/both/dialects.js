import * as JDB from "@hoolymama/jdb-utils";

import { Dialects, Transcripitions } from "/imports/both/collection";
import {
  assertUserAuthorized,
  assertOwner,
  assertDocExists
} from "/imports/both/assert";

Meteor.methods({
  getMyDialects() {
    assertUserAuthorized.call(this);
    const myDialects = Dialects.find({ ownerId: this.userId }).fetch();
    return myDialects;
  },

  getDialectPayload(id) {
  	// console.log("getDialectPayload: "+ id)
    const dialect = assertDocExists.call(this, Dialects, id);
 
    dialect.transcriptions = Transcripitions.find({ dialectId: id }).fetch();
        // console.log(dialect)

    return JDB.cloneWithoutIdsAndTimestamps(dialect);
  },

  createDialect(dialect) {
    console.log("CREATEDIALECT");
    console.log(dialect);

    assertUserAuthorized.call(this);
    const initDate = new Date();
    const dialectId = Dialects.insert({
      name: dialect.name,
      description: dialect.description,
      ownerId: this.userId,
      createdAt: initDate,
      updatedAt: initDate
    });

    dialect.transcriptions.forEach(t => {
      t.dialectId = dialectId;
      t.createdAt = initDate;
      t.updatedAt = initDate;
      Transcripitions.insert(t);
    });
    return dialectId;
  },

  updateDialect(dialect) {
     assertOwner.call(this, dialect.ownerId);
 
    assertDocExists.call(this, Dialects, dialect._id);
 
    const initDate = new Date();
    const doc = {
      name: dialect.name,
      description: dialect.description,
      updatedAt: initDate
    };
 
    Dialects.update({ _id: dialect._id }, { $set: doc });
    return dialect._id;
  },

  removeDialect(id) {
    const dialect = assertDocExists.call(this, Dialects, id);
    Dialects.remove(id);
    Transcripitions.remove({ dialectId: id });
  },

getTranscriptions(dialectId) {
	const dialect = assertDocExists.call(this, Dialects, dialectId);
  return Transcripitions.find({ dialectId: dialectId }).fetch();
 
},
  createTranscription(dialectId, transcription) {

    const dialect = assertDocExists.call(this, Dialects, dialectId);
     const initDate = new Date();
    transcription.dialectId = dialectId;
    transcription.createdAt = initDate;
    transcription.updatedAt = initDate;
    return Transcripitions.insert(transcription);
  },

  updateTranscription( transcription) {
    assertDocExists.call(this, Transcripitions, transcription._id);
    Transcripitions.update({ _id: transcription._id }, {
      $set: {
        updatedAt: new Date(),
        source: transcription.source,
        outcomes: transcription.outcomes
      }
    });
  },
  /* 
  note, the transcription may be orphan 
	in which case we delete it anyway - dont require dialect
	to exist.
	*/
  removeTranscription(id) {
    const transcription = assertDocExists.call(this, Transcripitions, id);
    const dialect = Dialects.findOne({ _id: transcription.dialectId });
    if (dialect) {
      assertOwner.call(this, dialect.ownerId);
    }
    Transcripitions.remove(id);
  }
});
