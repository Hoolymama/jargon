function assertUserAuthorized() {
  if (!this.userId) throw new Meteor.Error("not-authorized");
}

function assertOwner(ownerId) {
   assertUserAuthorized.call(this);
  if (ownerId !== this.userId) throw new Meteor.Error("not-authorized");
 }

function assertDocExists(collection, id) {
  const doc = collection.findOne(id);
  if (!doc) {
    throw new Meteor.Error("not-found");
  }
  return doc;
}

export { assertUserAuthorized, assertOwner, assertDocExists };
